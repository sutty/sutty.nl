---
title: Community Radios on Sutty
description: We're launching a new theme for alternative and independent media
author:
- Sutty
image:
  description: ''
  path: 
categories:
- Themes
order: 8
locales:
- 87fbfd69-fc64-441f-802d-93dd1feb353d
layout: post
uuid: cb42bc4f-ef83-4eca-b1f0-10ad3e5260e1
liquid: false
usuaries:
- 1
---


A new theme for hosting community radios is available on the Sutty.  We invite you to [visit the demo site](https://radio.sutty.nl/).

We developed this theme based on conversations we've been having with _compas_ from independent media, specially from [Red Nacional de Medios Alternativos](https://rnma.org.ar/) \[Alternative Media's National Network\].  It was presented on August 29, 2020, during the [17mo Encuentro de la Comunicación Comunitaria, Alternativa y Popular](https://www.instagram.com/p/CEdF80hJj5e/) \[17th Meeting for Community, Alternative and Popular Communication\], where we were invited to the Free Software panel.

## Features

By using this theme you can:

* Add all of your streaming addresses, so if one is down, the radio still works.

* The streaming doesn't stop when you browse the website.

* En celulares Android, al cambiar de app o apagar la pantalla, la radio sigue reproduciendo, ¡pueden escuchar la radio con el celular en el bolsillo!

* On Android phones, when you change apps or turn off the screen, the radio keeps playing, you can listen to the radio with the phone in your pocket!

* The radio retries transmission after connection outage.

* Publish articles with images.

* Add your logo, presentation, social networks and change colors so the theme adapts to your identity.

* Articles can be shared to social networks.

* Translate the site to different languages!

## We want to use it!

This theme is free and gratis to use, as well as Sutty.  You only need to [register in our panel](https://panel.sutty.nl/) and create your site following the instructions. We only ask you to follow our [terms of service](https://sutty.nl/en/terms-of-service/) and [code of conduct](https://sutty.nl/en/code-of-conduct/).  That means, community radios are invited as long as they're anti-opression 😁

## Solidarity Economy

Sutty is part of the solidarity economy and sustains itself by developing specific projects and voluntary donations.  We developed this theme with our _compa_ [Librenauta](https://copiona.com/) in 15 hours.  Our territory is in Buenos Aires, Argentina, and the Cyberspace as well.

We have lots of ideas on how to make this theme better and more useful and featureful, and you surely have your own too.

Our goal is that we all can collectively think how this theme and Sutty can be an ally of the alternative media, supporting our work as well.

We invite you to try it out and to reach out to us!


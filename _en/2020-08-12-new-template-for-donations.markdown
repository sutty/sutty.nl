---
title: New Template for Donations
description: As Sutty, we have designed a specific theme that facilitates requests
  for donations.
author:
- Sutty
image:
  description: ''
permalink: "/template-for-donations/"
categories:
- templates
order: 7
locales:
- c3665834-5164-4299-ba44-dbf1a7fd4c13
layout: post
uuid: de6f5d9c-a1ed-4e27-9de8-f012168dd429
liquid: false
usuaries:
- 5
---



In the context of Covid-19, certain community needs require immediate attention.

The pandemic as a catalyzer for structural inequality and injustice increases the urgency to compile, recover, and centralize requests for various donations.

Many groups, collectives, organizations, individuals, and actors in the social and solidarity economy need support.

This means that those who wish to spread the word about collaboration initiatives, donation requests, and other solidarity-based contributions can now create specific sites with ease on the Sutty platform.

Using this template, you will be able to:

* Describe the characteristics and motivations of the project, people, or initiatives in need of support;


* Specify alternative means of payment;


* Add payment buttons through the corresponding services.

To use it [register at our site](https://panel.sutty.nl/usuaries/sign_in) (we do not ask for personal data: just an email account). Once in the panel, you can create your site and choose the "Donations Template" to start.

If you are interested in having us display your site with other sites in a centralized peer support initiatives list at Sutty, you can message us through our [contact form](https://sutty.nl/index.html#contacto).

You can see how this could look at our own donations site, which is made with the template:

https://donaciones.sutty.nl/
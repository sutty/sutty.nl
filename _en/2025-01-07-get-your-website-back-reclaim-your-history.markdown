---
title: Get your website back, reclaim your history
description: 'Open Call: Resilience grant for NGOs, non-profits, and individuals.
  Recover a website you''ve lost and make it more resilient with the DWeb—100% free.
  Apply now!'
author:
- Sutty
- Distributed Press
- Hypha Coop
image:
  path: public/mnl6h9zbl6j35uqxgir4mqr29yrz/reclaim-your-site.jpeg
  description: Open Call. Resilience grant. Get your website back, reclaim your history.
    Distributed Press, Sutty, Hypha.
categories:
- campaña
- Distributed Web
tags:
- Campaign
- resilience grant
draft: false
activity: true
order: 16
locales:
- a31598a6-899b-46d8-b4bc-76d38d150456
layout: post
uuid: 96c7320f-bb72-45c8-8ca5-709034150976
render_with_liquid: false
usuaries:
- 5
created_at: 2025-01-07 00:47:34.544636759 +00:00
last_modified_at: 2025-01-14 21:41:02.343194072 +00:00
---

<p style="text-align:start">In the digital world, there’s nothing more disheartening than losing access to a website you’ve poured time and effort into building — a connection point with readers, referenced by links to articles and news :(</p>
<p style="text-align:start"></p>
<h3 style="text-align:start" id="lost-your-website-we-can-(maybe)-help!"><strong>Lost your website? We can (maybe) help!</strong></h3>
<p style="text-align:start"></p>
<p style="text-align:start">We're offering a Resilience Grant for NGOs, non-profits, and individuals who protect human rights and empower people, especially those facing systemic inequity and prejudice. We'll recover your site and make it more resilient with the DWeb—100% free.</p>
<p style="text-align:start"><a href="https://sutty.nl/en/recovery-form/" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">Apply now through this form</a>! We'll be choosing two organizations to work with in early January 2025.</p>
<p style="text-align:start"></p>
<h3 style="text-align:start" id="faqs"><strong>FAQs</strong></h3>
<p style="text-align:start"><strong>How will you recover my site?</strong></p>
<p style="text-align:start">We can restore your site if it meets certain technical conditions. <a href="https://sutty.nl/en/recovery-form/" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">Share your details in the registration form</a> so we can evaluate whether we can help you.</p>
<p style="text-align:start"><strong>How do you make my website more resilient?</strong></p>
<p style="text-align:start">At <a href="https://distributed.press/" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">Distributed Press</a>, we specialize in creating <strong>decentralized versions of websites using protocols like IPFS and Hypercore.</strong></p>
<p style="text-align:start">Having collaborative copies of your site hosted outside HTTP helps avoid fragility and link rot, reduces the risk of losing information, makes your data more resistant to censorship and deletion, and ensures it remains accessible even if/when hosting servers go down. As part of this grant, we’ll not only recover your site but also host it on the Distributed Web.</p>
<p style="text-align:start"><strong>What are the conditions?</strong></p>
<p style="text-align:start">We will recover 1 site for 2 organizations or individuals during February 2025. Selection criteria include technical feasibility of the restoration and your consent to share the story as a case study afterward. Due to the tight timeline of this project, you’ll also need to agree to a response time of around 48 hours — so we can move forward together!</p>
<p style="text-align:start"><a href="https://distributed.press/2025/01/02/recover-your-site-with-distributed-press/" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">Learn more about the conditions and requirements for recovering and preserving a</a></p>
<p style="text-align:start"><a href="https://distributed.press/2025/01/02/recover-your-site-with-distributed-press/" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">website in the Resilience Grant</a></p>
<p style="text-align:start"></p>
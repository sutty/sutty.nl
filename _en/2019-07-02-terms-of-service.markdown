---
title: Terms of service
author: Sutty
layout: post
description: "Which are the conditions for Sutty's users?"
permalink: terms-of-service/
categories:
- Sutty
uuid: a37dfaf7-7214-4d3e-a142-b9029ea04e02
locales:
- d5d3ca28-a8cb-426d-8930-e0b37db47230
---


> We use gender-neutral pronouns such as they/them to refer to people
> independently of their gender identities.

This document details Sutty's Terms of Service, including web site,
publishing platform, related infrastructure (chat rooms, etc.) and web
sites created by users through the platform.  From now on it will be
refered as "Sutty".

## This is too long!

* Sutty is developed for organizations, collectives, campaigns and
  individuals that are struggling to change the world :)

* Web sites promoting or inciting hate towards people for their body
  diversity, habilities, neuro-diversity, ethnicity, gender identities
  and expression, experiencie level, nationality, physical appearance,
  race, religion, age or sexual identity and orientation are definitely
  now allowed.

* Web sites promoting fascism are not allowed.

* Sutty doesn't monitor web site content, but reserves the right to
  remove those that don't follow these terms of service and/or code of
  conduct.

## Who are welcome to Sutty

We invite every organization, collective, activist, campaign,
cooperative, artist and other forms of grouping that promote diversity,
freedom of expression, trans-feminism, cooperativism, social economy,
free culture...

In case of doubt, [please contact us](index.html#contact).

## Who are not welcome to Sutty

Fascists, mysogynists, violents and hate groups are not welcome here.
[You can check our code of conduct](code-of-conduct/).

Sutty doesn't monitor the content of hosted web sites, we reserve the
right to remove any sites and users that fall in these categories and to
further block them to register new ones.

## Acceptable use

Sutty users can make use of the features of the offered services, in
accordance with the collective agreements gathered in the code of
conduct.

Users cannot host web sites that put the services and their
infrastructure at risk, specially those action that could put Sutty in
blacklists for activities such as spam, malware, phishing, etc.

Not complying with these conditions could lead to web site and related
user accounts removal.

## Limitation of liability

Sutty cannot be held responsible for the content of the web sites hosted
nor the personal opinion of users, and it doesn't monitor said content
in any way.

Even though backups are made internally and features for tracking
changes in web sites are provided, users are responsible for keeping up
to date backups of their sites and profile data.

## Web site and related user accounts removal

Not complying with this and other documents related to good conduct
and use of Sutty's offered services may result in web sites and
related user accounts removal.

Users will receive an explanation of the motives leading to removal.

This removal will be sole decision of Sutty's administrators.

## Free software and warranty limitation

Sutty is developed and published as free software under the terms of the
AGPLv3 license.  Other complements and content may be published under
different licenses.

This implies that every person has access to source code and can study,
modify and use it without warranty of fitness for a particular purpose.

[Access to source code and licensing
information.](https://0xacab.org/sutty/)

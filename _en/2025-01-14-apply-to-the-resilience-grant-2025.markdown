---
title: Apply to the Resilience grant 2025
description: The deadline for submissions has been extended by one more week, so there’s
  still time to apply to the Resilience Grant and have your website recovered and
  distributed by Distributed Press
author:
- Sutty
- Distributed Press
- Hypha Coop
image:
  path: public/qspy8064drh7w0gjqbufglkm5wqs/horizontalEN1.png
  description: ''
categories: []
tags: []
draft: false
activity: false
order: 17
locales:
- b551eb60-453d-47e1-926d-58aad4cbe8bb
layout: post
uuid: 62f9352c-5016-4b21-964a-7c08892f9d33
render_with_liquid: false
usuaries:
- 310
created_at: 2025-01-14 22:13:23.350845832 +00:00
last_modified_at: 2025-01-14 22:18:55.444213599 +00:00
---

<p style="text-align:start">Did you once have a website that disappeared over time? The Resilience Grant is here to help you bring it back and establish it on a stronger, decentralized foundation.</p>
<p style="text-align:start">If you are an NGO, non-profit, or an individual activist, this grant is designed to recover a website you have lost and fortify it with the power of the Distributed Web.</p>
<p style="text-align:start">The deadline for submissions has been extended by one more week, so there’s still time to apply. If you think you or someone you know might benefit, don't hesitate to apply through the <a href="https://sutty.nl/en/recovery-form" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">applications form</a>.</p>
<p style="text-align:start">Share this opportunity with others who might need it, and submit your application until January 20th!<br>
</p>
<figure><img src="public/08lz6d011n49noiy57h1jwiwec1a/cuadradoEN2.png" class="img-fluid" controls="false"></figure>
---
title: New Template for Publishing Projects
description: A new theme for the social and solidarity economy inspired by publishing
  projects
image:
  description: Image of the New Template for Publishing Projects, Tierra del Sur website
  path: public/aw83ttxww5zz0ninboxq0hq27z0e/Screenshot_2020-07-17 Tierra del Sur Editorial.png
categories:
- templates
order: 5
layout: post
uuid: 3caaf619-8dd7-48f0-ba23-f1ff3b2b54b1
liquid: false
usuaries:
- 5
uuid: ae0031b7-00a7-4313-a73d-9122869b759b
locales:
- 5f5f6c1f-979b-4b70-a161-84a8dbbc1488
---



As Sutty, we strive for collective solidarity projects can access the safest, most efficient, and lightest web technology available, based on Free Software and its ethical principles. In one of our feedback sessions with different collectives and designers, they proposed that we create templates (or themes) to meet their specific needs. These templates enable users to quickly address both design and communication needs, allowing these content creators to achieve their goals with more ease.

The template "Independent Editorial” was inspired by our friends at *[La Bastardilla](https://www.instagram.com/labastardillaa/),* a community-based and independent feminist market. As a result of the quarantine, they began to need digital spaces to display their publications. With this template, it is very simple to create a website to generate a catalog and associate a payment link that includes online payment methods.

Additionally, we focus on getting these catalogs to appear first in web searches.

To achieve this, we add a structured data vocabulary called schema.org that generates the necessary metadata structure for Google to recognize the items in the catalogue as objects for sale. When users search to purchase these books on the Internet, there will be a much better chance of reaching this website.

To use it, simply create an account in Sutty (we do not ask for your personal data: just an email account). Once in the panel, you will be able to create a site and choose the Independent Editorial from the template options.

Here are some sites which have been created using our template:

• [La Disgráfica](https://ladisgrafica.sutty.nl/)

• [Tierra del Sur](https://tierradelsur.sutty.nl/)

• [Kinky Vibe](https://kinkyvibe.sutty.nl/)

• [Sube la Marea](https://subelamarea.sutty.nl/)

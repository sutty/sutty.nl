---
title: We are launching Fediverse distributed publishing through Sutty!
description: Being able to share my website’s content on free and distributed social
  media automatically? Wow!
author:
- Sutty
image:
  path: public/vcl04pjsy61vykcwhtcttm6y9d5f/milestone_2_activity_pub_2.png
  description: Two toggles in colors for activating "Fediverse" and "Distributed Web"
categories:
- dWeb
- Distributed Web
- Fediverse
tags:
- launch
- dWeb
- Distributed Web
- Fediverse
- Jekyll plugin
- Activity Pub
- Social Inbox
- Distributed Press
- Hypha Coop
- tutorial
draft: false
activity: true
in_reply_to: https://hypha.coop/dripline/announcing-dp-social-inbox/
order: 14
locales:
- fb05c2ff-f92c-4652-b640-09373b34f15f
layout: post
uuid: db7b6085-dcae-4d2f-9c98-8aee65b8bfb7
render_with_liquid: false
usuaries:
- 5
created_at: 2023-12-04 22:07:18.312159703 +00:00
last_modified_at: 2024-06-19 13:40:51.916164864 +00:00
---

<p style="text-align:start">We are happy and proud to announce the launch of a new feature which allows for publishing articles from any Sutty website onto the Fediverse.</p><p style="text-align:start">Thanks to our joint work with <a href="https://distributed.press/" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">Distributed Press</a>, <a href="https://hypha.coop/" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">Hypha</a> and with the support of <a href="https://www.ffdweb.org/" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">Filecoin Foundation for the Distributed Web</a>, Sutty makes it possible to combine the security of your static website with the fast distribution of content that the free and descentralized social networks that make up the Fediverse allow for.</p><p style="text-align:start">This was made possible via the development and integration of two components, worked on in a joint and collaborative fashion:</p><ol>
<li><p style="text-align:start"><a href="https://github.com/hyphacoop/social.distributed.press" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">Social Inbox</a>, developed primarily by Distributed Press. It supplies the ability to recieve articles, reply to and mention other Fediverse accounts.</p></li>
<li><p style="text-align:start"><a href="https://0xacab.org/sutty/jekyll/jekyll-activity-pub" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">Jekyll Activity Pub Plugin</a>, developed primarily by Sutty. It makes it possible to integrate Social Inbox into every Jekyll-generated static website, allowing for the automatic publishing of site content to the Fediverse.</p></li>
</ol><p style="text-align:start"><a href="https://sutty.nl" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">Sutty</a> integrates the complete functionality into its CMS for static Jekyll websites, making it possible to administrate it from an interface which is continuously improving in usability.</p><p style="text-align:start">If you still aren’t familiar with these names and concepts, we invite you to learn more below, in the section named “For the techno-curious”.</p><h3 style="text-align:start" id="what-it-means">What it means</h3><ul>
<li><p style="text-align:start">If you create your website via Sutty, you now have new opportunities to disseminate your content and interact on digital networks.</p></li>
<li><p style="text-align:start">Your articles can now be published onto Fediverse social media.</p></li>
<li><p style="text-align:start">Your website will have a profile or user, customizable from the panel, on a Sutty instance unique to your website.</p></li>
<li><p style="text-align:start">Fediverse users will be able to follow your posts.</p></li>
<li><p style="text-align:start">Fediverse users that follow you will be able to read your posts, mention you and reply to you.</p></li>
<li><p style="text-align:start">You will be able to interact with Fediverse users via replies or mentions.</p></li>
</ul><h3 style="text-align:start" id="what-it-allows">What it allows</h3><ul>
<li><p style="text-align:start">Activating Fediverse publishing for any Sutty website.</p></li>
<li><p style="text-align:start">Activating (or not) publishing to the Fediverse for any individual article you want.</p></li>
<li><p style="text-align:start">Replying to comments with articles on your Sutty website.</p></li>
<li><p style="text-align:start">Customizing the account that Sutty automatically creates for your site on the Fediverse.</p></li>
<li><p style="text-align:start">Reporting or informing about abusive users or instances using our <a href="https://sutty.nl/en/index.html#contact" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">contact form</a>.</p></li>
</ul><h3 style="text-align:start" id="what's-next">What’s next</h3><ul>
<li><p style="text-align:start">Improvements in integrating replies as in-website comments.</p></li>
<li><p style="text-align:start">Incoporation of <em>mentions</em> from the Sutty panel.</p></li>
<li><p style="text-align:start">Improvements in the general interface of the panel.</p></li>
<li><p style="text-align:start">New features to improve moderation.</p></li>
<li><p style="text-align:start">Better compatibility with other Fediverse networks (Mastodon+Glitch, Pleroma, Ktistec).</p></li>
<li><p style="text-align:start">Differentiation of content to be published onto the Fediverse and onto the Sutty website.</p></li>
<li><p style="text-align:start">The possibility to export your account to another Fediverse intance via the Sutty panel.</p></li>
<li><p style="text-align:start">The possibility for Sutty to announce your content and/or Fedi user automatically to attract followers. (Right now, you can do it <a href="https://cryptpad.fr/form/#/2/form/view/yp1KZwQjgU2RG-zhdQCyw4M8QhftNCVu8e+IJG2iN7Y/" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">through this form</a>).</p></li>
<li><p style="text-align:start">Access to your follower and following list via the panel.</p></li>
<li><p style="text-align:start">Follow, unfollow, block users and/or Fediverse instances from the panel.</p></li>
</ul><h3 style="text-align:start" id="i-want-to-use-it!">I want to use it!</h3><p style="text-align:start">You are invited to try it out using <a href="https://sutty.nl/en/how-to-publish-your-sutty-posts-in-fediverse/" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">our tutorial.</a></p><h3 style="text-align:start" id="for-the-techno-curious">For the techno-curious</h3><h3 style="text-align:start" id="how-it-works">How it works</h3><p style="text-align:start">Websites and social media may seem like different species inside the universe of the Internet. At the same time, corporate and concentrated social media such as Instagram, Facebook, X (ex-Twitter), among others, have proven to be hostile with certain groups and communities in particular (censoring content, persecuting nipples, hiding posts due to skin color, hair color, etc.) and with its users in general (massive commercialization of data, violating privacy agreements, choosing interface use and design that generates anxiety and addiction, etc.). Despite this, their use is still practically compulsory in order to advertise a person’s work or spread urgent news.</p><p style="text-align:start">The Fediverse is a federated, decentralized, and distributed network of free and open source social media, each with its own characteristics, preferences, and user groups. They are designed to allow dialogue between all of them. This means the content is visible can be replied to from many different platforms, strengthening a culture of participation and diversity of voices, based on the standards of free (libre) development that seek to be ethical moreso than make endless revenue.</p><p style="text-align:start">Websites are still a format for media that, because of their characterstics, favor the dissemination of content such as multimedia articles. They allow us to customize a style or visual identity, maintain sections and diverse institutional content, for example.</p><p style="text-align:start">Social media favors <em>immediacy</em>, allowing a flow of dialogue <em>in real time</em> that contemplates a different span of attention and navigation characteristics which make it shorter, faster, and sometimes ephimeral. Communication media tends to use them to call attention on content published on their websites, betting on rapid exposure and the discussions that can be had amongst users.</p><p style="text-align:start">The feature we have developed here at Sutty contemplates use cases in which one wants to share content to more people, in less time, and with the possibililty of opening dialogue. The particularities of our websites and of free and open source social media generate favorable conditions for free speech, which we prefer to call the Right to Communication, evading the diverse and continously more sophisticated forms of censorship of traditional corporate platforms. For content to be reproduced in several places at once favors its dissemination and is ideal for those counterhegemonic voices and dicourses to exist on the web and survive the passage of time, preserving the memory of the community.</p><h3 style="text-align:start" id="how-moderation-works-in-the-fediverse">How moderation works in the Fediverse</h3><p style="text-align:start">The Fediverse attempts to function as interconnected online communities which are self-governed in a way which strives for collective care. Each instance could be something like a town which houses different accounts or users under norms which have been negotiated/agreed upon by its users and that may be up for discussion if necessary. This way, it is possible to regulate the circulation of fascism and hate speech which may damage not only the participation of certain users but also their health.</p><p style="text-align:start">In order to do this, each instance chooses how it practices moderation and may exclude other instances with previous reports of content which is antidemocratic, hateful or contrary to the values and care of its inhabitants.</p><p style="text-align:start">At Sutty in particular, we are interested in strategies and mechanisms for collective care, which is why we are continuously designing models that allow us to uphold them in our technologies. You can read our <a href="https://sutty.nl/en/terms-of-service/" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">Terms of Service</a>, <a href="https://sutty.nl/en/privacy-policy/" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">Privacy Policy</a>, and <a href="https://sutty.nl/en/code-of-conduct/" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">Code of Conduct</a> for more information.</p><h3 style="text-align:start" id="are-you-interested-in-participating">Are you interested in participating?</h3><p style="text-align:start">If you are part of a social organization, activism group, or social collective which you think would benefit from these characteristics, we invite you to <a href="https://sutty.nl/en/index.html#contact" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">contact us</a>. We are looking for ways to improve the use of technology for you and value your experiences.</p><h3 style="text-align:start" id="possiblities-of-integration-of-social-inbox-with-other-static-websites">Possiblities of integration of Social Inbox with other static websites</h3><p style="text-align:start">If you are interested in incorporating this feature for other static website managers, don’t hesitate to <a href="https://sutty.nl/en/index.html#contact" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">contact us</a>. You can also keep up with the latest developments at <a href="https://dweb.sutty.nl" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">https://dweb.sutty.nl</a> and our blog <a href="https://sutty.nl/blog" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">https://sutty.nl/blog</a>.</p><h3 style="text-align:start" id="recommendations-if-you-are-interested-in-learning-more">Recommendations if you are interested in learning more</h3><ul>
<li><p style="text-align:start">MeetUp presentation: <a href="https://hackmd.io/3xsFFy-RSwWJnPjFKUco1w?view#/1" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">https://hackmd.io/3xsFFy-RSwWJnPjFKUco1w?view#/1</a></p></li>
<li><p style="text-align:start">Our blog in which we collect info on the distributed web and share in Spanish: <a href="https://dweb.sutty.nl" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">https://dweb.sutty.nl</a></p></li>
<li><p style="text-align:start">Our general blog: <a href="https://sutty.nl" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">https://sutty.nl</a></p></li>
<li><p style="text-align:start">Jekyll Activity Pub Plugin blog: <a href="https://jekyll-activity-pub.sutty.nl/" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">https://jekyll-activity-pub.sutty.nl/</a></p></li>
<li><p style="text-align:start">Mauve’s blog and article on Social Inbox: <a href="https://blog.mauve.moe/posts/distributed-press-social-inbox" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">https://blog.mauve.moe/posts/distributed-press-social-inbox</a></p></li>
</ul>
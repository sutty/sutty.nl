---
title: Distributed publication is available through Sutty!
description: Join us in creating a new Web built on cooperation
author: []
categories: []
tags: []
draft: false
order: 10
locales:
- cc9fc862-bc29-45da-b427-6909ae14611f
layout: post
uuid: d0afe144-e46a-4560-9e9e-492b0f03d23a
liquid: false
usuaries:
- 1
- 310
- 417
last_modified_at: 2023-04-26 20:35:00.340840346 +00:00
---

<h5 style="text-align:start" id="join-us-in-creating-a-new-web-built-on-cooperation"><strong>Join us in creating a new Web built on cooperation</strong></h5><p style="text-align:start"><br>What made the internet incredible in its early days were not the videos of kittens throwing things off a table. All of the content that entertains, scares, or angers us today, arises from technological advancements that made possible something unthinkable before the 20th Century: that people could link directly with others to send or receive files, unbounded by space and time.</p><p style="text-align:start">From that turning point in 1969 that connected the first four university nodes to this day, some important things were lost. The architecture of this huge global network has bottlenecks: most of its&nbsp; infrastructures have become centralized. We send our information through these centralized channels,and thus hand over our data to powerful companies and governments, enabling them to control and surveil our online presence, interactions, and transactions.<br></p><p style="text-align:start">If you are an activist, journalist, or artist, you are probably accustomed to constantly looking for innovation and transformation zones. A mixture of curiosity and necessity to find out where really new and relevant spaces are being created in the social fabric.&nbsp;</p><p style="text-align:start">The distributed web is one such movement. It aims to re-create the peer-to-peer (P2P) practices that made the internet a true socio-technical revolution. To return to a collective distribution of information, out of the commercial highways that capture us.&nbsp;</p><p style="text-align:start">Have you lost your online presence because of broken links or lost domains?</p><p style="text-align:start">The distributed web seeks to solve that by making content available through a variety of means.&nbsp;</p><p style="text-align:start">Do you have material designed for zones of very low connectivity?</p><p style="text-align:start">The distributed web can improve your accessibility for even those in offline communities where they have no access to the internet.</p><p style="text-align:start">Looking to use innovative technologies that are designed with the human right to communicate and resilience in mind?</p><p style="text-align:start">The distributed web strives for this very goal.</p><p style="text-align:start">The Sutty CMS is integrating with Distributed.Press to make it easy to publish to the distributed web thanks to the generous support of <a href="https://ffdweb.org/" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">Filecoin Foundation for the Decentralized Web (FFDW</a>). With the new DWeb toggle, you can make a site that can be browsed in https: but also through P2P protocols, InterPlanetary File System (IPFS) and Holepunch. You can use it now to create content that is more resilient and available offline. <a href="https://sutty.nl/en/learn-more-about-publish-to-dweb-functionality/" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">Find out more</a> about the Distributed.Press toggle</p><p style="text-align:start">Sign-up today and participate in the creation of a new web, which builds its power through cooperation. Start here: <a href="https://sutty.nl" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">sutty.nl</a></p><figure><img src="public/3h0r0nyfsbfe5vujstuw0wf34zs2/footerEN.png" class="max-w-100" controls="false"></figure><p style="text-align:start"></p>
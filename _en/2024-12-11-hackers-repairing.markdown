---
title: Hackers repairing
description: We share with you an analysis of our system conducted from India, discovering
  some points to strengthen to stay invulnerable to malicious attacks.
author: []
categories: []
tags: []
draft: false
activity: true
order: 14
locales:
- 20e85b98-d63c-4bfc-a14a-29d20b230e06
layout: post
uuid: '04834ba5-3a13-44a9-ae89-278292e2cba1'
render_with_liquid: false
usuaries:
- 310
created_at: 2024-12-11 19:25:00.862944542 +00:00
last_modified_at: 2024-12-11 19:25:58.012128708 +00:00
---

<h5 style="text-align:start" id="in-the-world-of-free-software-such-nice-and-curious-things-happen-that-it's-right-to-talk-about-it.-a-world-where-you-get-an-email-from-a-hacker-and-it's-going-to-be-good-news.">In the world of free software, such nice and curious things happen that it’s right to talk about it. A world where you get an email from a hacker, and it’s going to be good news.</h5>
<p style="text-align:start">Here’s what happened:</p>
<p style="text-align:start"><a href="https://scriptjacker.in/" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">Parth Narula</a> wrote to us from India in early September, asking for permission to do an analysis of our platform, which we gave. And he found two vulnerabilities that we could improve. In both cases, they are not security or performance issues that harm our users due to our actions, but they are points in our system that a malicious actor could misuse to perform attacks such as flooding their mailboxes with emails, or inviting a user to download malware.</p>
<p style="text-align:start">Because <mark style="color:inherit;background-color:#cbfbf5">in the quest to offer resilient and secure software</mark>, we also have to <mark style="color:inherit;background-color:#cbfbf5">anticipate all the different paths that malicious actors could create</mark> to cause damage and seek compensation. In our domain, a bit of paranoid thinking is more than useful ;)</p>
<p style="text-align:start">So Parth’s analysis and recommendations led us to quickly review and resolve three points of our CMS dashboard to prevent them from being vulnerable to malicious attack.</p>
<ol>
  <li>
    <p style="text-align:start">We improved the protection against “brute force”, which prevents a type of attack where millions of passwords are tried on us, but at the same time allowing users to make a normal use of the login, make a mistake when typing it and retry.</p>
  </li>
  <li>
    <p style="text-align:start">We improved protection against sending email confirmations and password unlocks, preventing a type of attack that is done by asking for this process many, many times and flooding the mailbox with unsolicited email.</p>
  </li>
  <li>
    <p style="text-align:start">We limit the incorporation of links in an unnecessary space on our sites, which would allow a malicious hyperlink mailing attack to unsuspecting people, potentially turning us into a phishing platform.</p>
  </li>
</ol>
<p style="text-align:start">Specifically, the Sutty CMS panel is our most sensitive piece of security because it is our only dynamic part. This means, in plain English, that it generates content on the go, depending on databases and server-side procedures: complex processes that can be exploited if they are not well protected.</p>
<p style="text-align:start">Its security requires additional measures such as access controls, input validation and continuous monitoring. This is why all the websites we make are, instead, “static” or “solid”, as interactive and beautiful as any other but with a technical solution that simplifies how they are generated on the server and drastically reduces vulnerabilities to the most common Internet attacks, such as automated infection with malware, advertising, webshellz and even, as they use fewer resources, increasing the cost of doing denial of service (DOS) attacks.</p>
<p style="text-align:start">Other actions that constitute our approach to cyber security include having multiple servers up and running, being very obsessive about not requesting or storing personal information, and making encrypted, remote backups of everything. What we do in our infrastructure is public here: <a href="https://0xacab.org/sutty/ansible" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">https://0xacab.org/sutty/ansible</a> and here <a href="https://gitea.nulo.in/Sutty/-/packages" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">https://gitea.nulo.in/Sutty/-/packages</a>.</p>
<p style="text-align:start">In addition, all changes that Sutty users make to their sites are registered in a way that can be undone. This means that if someone gains access to the manager and makes improper changes, we can help you undo it. Improper access can occur either by simple issues such as leaving open the session on a shared computer or more complex, such as an attack that exploits a vulnerability in the platform or uses social engineering techniques (that is, it is not a technical issue but a deception for a person to give the access keys believing that he/she is leaving them to another trusted person).</p>
<h5 style="text-align:start" id="thanks-to-free-software-and-the-activities-of-ethical-hackers-or-hacktivists-the-digital-tools-we-use-can-be-more-agile-more-secure-more-sovereign-and-resilient-day-by-day.-we-are-very-grateful-to-parth-for-his-constructive-and-well-intentioned-contribution."><strong>Thanks to free software and the activities of ethical hackers or hacktivists, the digital tools we use can be more agile, more secure, more sovereign and resilient day by day. We are very grateful to Parth for his constructive and well-intentioned contribution.</strong></h5>
<h5 style="text-align:start" id="parth-narula-is-founder-of"><strong>Parth Narula is founder of <a href="https://scriptjacker.in/" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">ScriptJacker</a>, a dedicated cybersecurity team that provides research, outreach and advice on OWASP, red teaming, ethical hacking and pentesting. They share resources, case studies and learning materials on his site. You can also contact via <a href="https://x.com/scriptjacker" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">X</a>, <a href="https://www.linkedin.com/in/parth-narula-86283821a?original_referer=https%3A%2F%2Fwww%2Eecosia%2Eorg%2F&amp;originalSubdomain=in" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">Linkedin </a>and <a href="https://github.com/scriptjacker" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">Github </a>!</strong></h5>
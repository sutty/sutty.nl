# frozen_string_literal: true

Jekyll::Hooks.register :site, :post_read, priority: :high do |site|
  site.config['activity_pub_disable'] = !site.default_locale?
end

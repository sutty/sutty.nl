---
title: Política de Privacidad
author:
- Sutty
layout: post
description: ¿Cuáles son los cuidados de este sitio con respecto a sus usuaries y visitantes?
permalink: politica-de-privacidad/
categories:
- Sutty
uuid: a6c94a0c-1bfd-4fe3-9555-40448aa18e2b
locales:
- 7a6f73f2-b199-4de6-8348-a026782cd2c9
---

> Utilizamos la e como pronombre neutro para referirnos a personas
> independientemente de su identidad de género, por ejemplo “usuarie”.

Este documento detalla la política de privacidad de Sutty, incluyendo
sitio web, plataforma de edición, infraestructura relacionada (salas de
chat, etc.) y sitios creados por sus usuaries a través de la plataforma,
en adelante "Sutty".

## ¡Esto es demasiado largo!

Un resumen:

* Sutty no recolecta datos personales de ningún tipo

* Sutty solo recolectaría datos analíticos que no identifican a
  personas

## Datos analíticos

La única recolección de datos realizada por Sutty es con fines
analíticos (cantidad de visitas, duración, etc.), no asociados a datos
personales.

Los datos analíticos recolectados por cada sitio podrán ser utilizados
internamente por Sutty.  Sutty no comparte datos analíticos con
terceros en forma privada.  Datos analíticos seleccionados podrán ser
utilizados públicamente.

Sutty no recomienda la recolección de datos personales de ninguna forma,
pero no monitorea que les usuaries utilicen servicios de terceros con
sus propias políticas de privacidad.  Recomendamos a les usuaries y
visitantes informarse antes de utilizar servicios de estadísticas de
terceros.

## No registro de datos personales

Sutty no registra direcciones IP de usuaries ni de visitantes de ninguna
forma.

Sutty no solicita datos personales para el registro de cuentas de
usuarie en su plataforma.

Sutty solo utiliza “cookies” de sesión para identificar usuaries
mientras utilicen la plataforma.  No se utilizan “cookies” para
identificar visitantes a los sitios alojados por Sutty.

El único caso en el que Sutty podría solicitar datos personales es
durante el pago de servicios.  Se tomarán medidas de seguridad digital
para salvaguardar esta información y descartar lo que sea posible una
vez que ya no sea necesaria.

Se notificará a les usuaries cuando su información personal sea
eliminada.

Si les usuaries deciden alojar sus sitios con terceros, deberán
informarse de las políticas de privacidad correspondientes.  Sutty
recomienda servicios de terceros con políticas de privacidad coherentes
con estas.

---
title: Jornada de presentación de Sutty, Taller e Intercambio
description: Les invitamos a encontrarnos este martes 11 de febrero en el Centro Cultural Tierra Violeta (Tacuarí 538, San Telmo, CABA, Argentina), de 17 a 20.00 para conocer y probar Sutty.
author:
- Sutty
image:
  description: Flyer con la información de la jornada de presentación de Sutty en Tierra Violeta
  path: public/m8ykhzw2bwqts7aw1pagwd4duneg/text1241.png
categories:
- Taller
tags:
- taller
- jornada
- presentación
order: 7
layout: post
uuid: b79cd2d4-2f72-4d52-a11f-0e021d4d62ab
liquid: false
usuaries:
- 1
---

**¡Importante! La fecha es martes 11/2**

Existe una nueva plataforma para crear y publicar sitios web, pensada especialmente para y desde prácticas transfeministas, ecologistas y de cuidados digitales.

Les invitamos a encontrarnos este **martes 11 de febrero en el** [**Centro Cultural Tierra Violeta (Tacuarí 538, San Telmo, CABA, Argentina)**](geo:-34.6146013,-58.3788942), **de 17 a 20.00 para conocer y probar Sutty.** Pero sobre todo, nos interesa saber cómo les resulta usarlo, si sienten que es accesible, si les dan o no ganas de hacer sus sitios, si les sirve para sus campañas o para visibilizar sus luchas.

Podés leer los códigos para compartir que acordamos en nuestros eventos: [https://sutty.nl/codigo-de-convivencia/](https://sutty.nl/codigo-de-convivencia/)

Flyer ilustrado por [Lauti Attila](https://www.instagram.com/lauti_attila/)

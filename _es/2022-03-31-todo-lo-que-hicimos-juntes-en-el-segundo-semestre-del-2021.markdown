---
title: Todo lo que hicimos juntes en este segundo semestre del 2021
description: Informe semestral de Sutty 2021.
author: []
image:
  description: ''
categories: []
tags: []
draft: false
order: 23
locales: []
layout: post
uuid: ba84aa34-3208-4ce2-9e30-fa9fabe6ee51
liquid: false
usuaries:
- 417
last_modified_at: 2022-07-13 20:34:03.983971758 +00:00
---

<p>En Sutty estuvimos haciendo muchas cosas la última parte del año 
pasado. Como siempre, nos gusta compartirlas para dar a conocer los 
proyectos en los que trabajamos y los que trabajan con nosotres.</p><h2>Movimientos de la cooperativa</h2><figure data-multimedia=""><img data-multimedia-inner="" src="https://panel.sutty.nl/rails/active_storage/blobs/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBBaDhzIiwiZXhwIjpudWxsLCJwdXIiOiJibG9iX2lkIn19--8188c29a55ef5411e141c0d3f46c647e25f0be19/upload_12ec08c81f837d40e8137b4b97a3654d.png" alt='Un banner con el texto "movimientos de la cooperativa" en azul'></figure><ul>
<li>
En Agosto 2021 recibimos la aprobación del INAES de nuestra <strong>constitución como cooperativa</strong>, gracias al acompañamiento de <a href="http://tes.coop.ar">Trabajando por la Economía Social (TES)</a>
</li>
<li>
Nos sumamos como <a href="https://facttic.org.ar/sutty/"><strong>socies de FACTTIC</strong></a>, la Federación Argentina de Cooperativas de Trabajo de Tecnología, Innovación y Conocimiento.
</li>
<li>
En diciembre fuimos <strong>ponentes en la <a href="https://blog.archive.org/2022/01/05/dweb-holiday-fair-december-2021-meetup-recap/">DWeb Fair</a></strong>, una red global de <strong>constructores de la web descentralizada</strong>.
 En su evento anual "DWeb Fair" reunieron + de 30 proyectos, 
fundaciones, start-ups, organizaciones y protocolos que compartieron su 
visión de la web descentralizada.
¡Sutty estuvo entre elles! y tuvimos el placer de contar lo nuestro y 
escuchar y debatir ideas con compas de Filecoin Foundation, Internet 
Archive, ImageSnippets, SHER, Skynet Labs... entre otres.
</li>
<li>
El 17 y 18 de diciembre participamos del <a href="https://www.youtube.com/watch?v=cXY9_iiFNQU"><strong>Plenario anual de FACTTIC</strong></a>, junto con casi 100 cooperativistas de todo el país en representación de las 30 cooperativas que conforman la Federación.
</li>
</ul><figure data-multimedia=""><img data-multimedia-inner="" src="https://panel.sutty.nl/rails/active_storage/blobs/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBBaUFzIiwiZXhwIjpudWxsLCJwdXIiOiJibG9iX2lkIn19--7b97e34bffad46e3e05d9d0e47ca5fa3524a2fe4/upload_c47e529a1e75d36098fe8f08331678ac.png" alt='Un banner con el texto "desarrollos para clientes" en rosa'><figcaption></figcaption></figure><h2>Desarrollos para clientes</h2><figure data-multimedia=""><img data-multimedia-inner="" src="https://panel.sutty.nl/rails/active_storage/blobs/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBBbEFzIiwiZXhwIjpudWxsLCJwdXIiOiJibG9iX2lkIn19--ceb741556d85e9c197c00b17a6567499973de6ef/Capturas%20juntas.jpg" alt="Varias capturas de pantallas de los sitios que hicimos para nuestrxs clientxs"><figcaption></figcaption></figure><ol>
<li>
Mejoras y mantenimiento de <a href="https://tintalimon.com.ar/"><strong>Tinta Limón</strong></a><strong> y <a href="https://la-periferica.com.ar/">La Periférica</a></strong>, proyectos desarrollados en colaboración con la <a href="https://cooperativadedisenio.com/">Cooperativa de Diseño</a>.

Para Tinta Limón creamos la 
funcionalidad de "donación" asociada a un libro o un artículo, que 
permite a les visitantes aportar dinero sin realizar una compra, 
simplemente como incentivo o reconocimiento a la producción.</li>
<li>
Desarrollo de
<a href="https://sobreviviraldescalabro.org/">Sobrevivir al Descalabro (SAD)</a>. Es una <strong>serie-web de libre acceso</strong>. Se compone de entrevistas audiovisuales a personalidades de distintos ámbitos y latitudes, destacadas en temas de <strong>transición ecológico-social, pensamiento decolonial, buen vivir</strong>,
 políticas y pensamientos decrecentistas, y escenarios de colapso de la 
sociedad productivista. El desarrollo fue hecho en 
colaboración con la <a href="https://cooperativadedisenio.com/">Cooperativa de Diseño</a>.

</li>
</ol><figure data-multimedia=""><img data-multimedia-inner="" src="https://panel.sutty.nl/rails/active_storage/blobs/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBBbEVzIiwiZXhwIjpudWxsLCJwdXIiOiJibG9iX2lkIn19--ce40e243a6e474943fe1327d2499f5e541e83417/Capturas%20juntas2.jpg" alt="Varias capturas de pantallas de los sitios que hicimos para nuestrxs clientxs, Keywords, Circle y The Bridge Collective"><figcaption></figcaption></figure><ol>
<li>
<strong><a href="https://keywords.nyupress.org/">Keywords</a> para New York University Press</strong>.
Un importante trabajo de migración y clasificación de contenidos realizado para la NYU, a pedido de <a href="https://wp.nyu.edu/library-dlts/">NYU Digital Library Technology Services</a>.
 "Keywords" es de la editorial de la universidad, y Sutty trabajó en la 
migración de su sitio Wordpress manteniendo su diseño y contenido: ¡más 
de 11 mil artículos!

</li>
<li>
Creación del mapa colaborativo <a href="https://mapeando-diploesi.sutty.nl/"><strong>Mapeando DiploESI</strong></a>.
Mapa colaborativo para mostrar los distintos <strong>proyectos de Educación Sexual Integral</strong> elaborados a partir de la Diplomatura en ESI 2020- 2021, por el Colectivo Mariposas Mirabal, Universidad de Buenos Aires.

</li>
<li>
Construcción del sitio de <a href="https://encirculo.org/es/"><strong>Círculo</strong></a> para <strong><a href="https://articulo19.org/">Artículo 19</a> y <a href="https://guardianproject.info/">Guardian Project</a></strong>.
Círculo es una <strong>aplicación que ofrece un canal seguro de comunicación</strong> entre personas y sus contactos, para que, al enfrentar <strong>situaciones de riesgo</strong>, la red de apoyo implemente los <strong>protocolos de cuidados pertinentes</strong>.
 Círculo se creó desde organizaciones de la sociedad civil preocupadas 
por los derechos humanos, la privacidad digital y la seguridad física de
 las personas que ocupan un rol central en las democracias.

</li>
<li>
Maquetación de <a href="https://thebridgecollective.org">thebridgecollective.org</a>.
The Bridge Collective es una organización dedicada a <strong>proveer apoyo práctico</strong> para personas que necesiten acceder a <strong>abortos seguros o recursos asociados a su salud sexual y reproductiva</strong> en Texas, EE.UU. En este sitio también trabajamos en colaboración con Cooperativa de Diseño.

</li>
<li>El <a href="https://gitea.nulo.in/Nodemecu/ectomobile">Sistema Modular para la Captura de Datos y Muestras Ambientales
('nodemecu')</a> es un desarrollo conjunto de <a href="https://buenvivientes.org/">Herramientas para el Buen
Vivir A.C.</a> (México), la cooperativa de desarrollo digital Sutty
(Argentina), BarraDev infraestructuras y telemetría (Argentina) y la
<a href="https://ptga.com.ar/">Cooperativa de Producción Tecnológica, Gráfica y Audiovisual LTDA</a>
(Argentina) con el apoyo económico de <a href="https://www.witness.org/">Witness</a> (USA), como parte del
acompañamiento realizado al Frente de Pueblos en Defensa de la Tierra
y el Agua de Puebla, Morelos y Tlaxcala en el marco del proyecto de
fortalecimiento “Coral”.</li>
</ol><figure data-multimedia=""><img data-multimedia-inner="" src="https://panel.sutty.nl/rails/active_storage/blobs/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBBaWdzIiwiZXhwIjpudWxsLCJwdXIiOiJibG9iX2lkIn19--66a65b6d752b08b9ab42ba684670e8079ce43434/upload_5a0862ad2978fd555f68889495678724.png" alt='Un banner con el texto "desarrollos para la comunidad" en celeste'><figcaption></figcaption></figure><h2>Desarrollos para la comunidad</h2><p>Les compartimos algunas producciones que fuimos haciendo para <strong>trabajar más eficazmente</strong>, <strong>facilitar recursos</strong> al público en general, o <strong>funcionalidades nuevas</strong> que incorporamos a nuestra plantilla base. Las nuevas funcionalidades que diversos clientes encargaron y financiaron son <strong>replicadas luego en la gestora de sitios web Sutty en forma gratuita</strong>, por lo cual quienes nos eligen saben que están realizando un <strong>aporte solidario a una comunidad más grande</strong>.</p><ul>
<li>
<a href="https://0xacab.org/sutty/haini.sh"><strong>Haini.sh</strong></a>: proyecto para generar un entorno de desarrollo reproducible. Permite 
instalar y configurar de una sola vez todas las dependencias necesarias 
para empezar a codear (Ruby, Jekyll, etc). Esto permite instalar el 
entorno de Sutty de forma muy simple.

</li>
<li>
<a href="https://certificado.sutty.nl/">Sitio de los <strong>certificados Let's Encrypt</strong></a>.
En octubre de 2021, los navegadores de sistemas operativos no 
actualizados, como Windows 7, Vista y XP, dejaron de reconocer los 
certificados validados por Let’s Encrypt. Creamos un sitios para 
facilitar el acceso e instalación de certificados Let’s Encrypt y 
recuperar acceso a millones de sitios web.

</li>
<li>Incorporación de la <strong>tienda</strong> en "sutty-base", nuestra 
plantilla de trabajo, permitiendo a todas las personas que eligen 
nuestra gestora de sitios web incorporar la funcionalidad de carrito de 
compras.
</li>
<li>
Implementación de <strong>Stripe</strong> como método de pago en la tienda y módulo de donaciones/crowdfunding.
</li>
<li>
Implementación de <strong>estadísticas</strong> en el panel de los sitios web.
</li>
</ul><figure data-multimedia=""><img data-multimedia-inner="" src="https://panel.sutty.nl/rails/active_storage/blobs/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBBaWtzIiwiZXhwIjpudWxsLCJwdXIiOiJibG9iX2lkIn19--5eff1053a819ac33e12b7e1683bb09b54b217881/upload_e1304f5da0cb3c61f44e988c5a3ff6fd.png" alt='Un banner con el texto "colaboraciones con el software libre" en rosa'><figcaption></figcaption></figure><h2>Colaboraciones con el Software Libre</h2><p>Algunos de los pull requests (colaboraciones) que realizamos y fueron aprobados este semestre en proyectos de Software Libre:</p><ul>
<li><a href="https://gitlab.alpinelinux.org/alpine/aports/-/merge_requests/27728">Alpine Linux: actualización de seguridad a Ruby</a></li>
<li><a href="https://github.com/spree-contrib/spree_i18n/pull/837">Spree: actualizar traducciones al castellano</a></li>
<li><a href="https://github.com/afair/email_address/pull/72">Email_address: soporte para otros lenguajes aparte de inglés</a></li>
<li><a href="https://github.com/philnash/jekyll-gzip/pull/8">Jekyll-gzip: comprimir archivos JSON</a></li>
<li><a href="https://github.com/philnash/jekyll-brotli/pull/3">Jekyll-brotli: comprimir archivos JSON</a></li>
<li><a href="https://github.com/jekyll/jekyll/pull/8424">Jekyll: acelerar la compilación de sitios</a></li>
<li><a href="https://github.com/ashmaroli/jekyll-data/pull/38">Jekyll-data: arreglar incompatibilidad con Jekyll 4.2</a></li>
<li><a href="https://github.com/deksai/pam_abl/pull/5">Pam_abl: arreglar problemas de compilación</a></li>
<li>
<a href="https://github.com/activerecord-hackery/ransack/pull/1149">Ransack</a> y <a href="https://github.com/discourse/prometheus_exporter/pull/132">Prometheus_exporter</a>: arreglar manejo de strings</li>
</ul><p>Participamos del ciclo Puntapie inicial de FACTTIC, brindando un 
taller sobre Jekyll para otras cooperativas de tecnología. Pueden ver 
algunos fotogramas <a href="https://twitter.com/SuttyWeb/status/1437762831668105219">aquí</a>.</p><p>Participamos del ciclo Computación Cotidiana con Software Libre del 
Curso de Programación para Organizaciones Comunitarias. Brindamos un 
taller sobre creación de páginas web con Software Libre a través de 
nuestra gestora Sutty, del que pueden escuchar un fragmento <a href="https://www.instagram.com/tv/CUYhfdDAx5-/">aquí</a>.</p><p>Durante los primeros meses de este año ya, hicimos muchas otras cosas 
que estamos ansioses de contarles. Pueden seguirnos en <a href="https://todon.nl/@sutty">el Fediverso</a>, <a href="https://twitter.com/SuttyWeb">Twitter</a>, <a href="https://t.me/suttygestoraweb">Telegram</a> e <a href="https://instagram.com/sutty.web">Instagram</a> para enterarse de todo lo que vamos haciendo. ¡Nos vemos!</p>
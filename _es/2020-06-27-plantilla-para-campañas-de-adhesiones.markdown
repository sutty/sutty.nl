---
title: Plantilla para Campañas de Adhesiones
description: Una nueva plantilla de Sutty diseñada por Librenauta para la colecta
  de adhesiones.
author:
- Sutty
image:
  description: ''
categories:
- Plantillas
tags:
- Adhesiones
- Jornada de Intercambio
order: 14
layout: post
uuid: e2ccb087-8af7-4324-b0bb-4edbe5282ec9
liquid: false
usuaries:
- 5
- 1
---



Ya está disponible la plantilla para sumar adhesiones en el marco de campañas o iniciativas que requieran de apoyo colectivo y demostración de fuerzas.

Usando esta plantilla, podrán crear rápidamente un sitio web en el que podrán:

* publicar el documento;

* tener un formulario donde les visitantes/adherentes podrán sumar su adhesión, junto con otra información personal, organizacional y/o de contacto que considerara relevante, incluyendo su logo;

* contar con una muestra de las firmas recolectadas hasta el momento, que pueden imprimir;

* compartir su campaña a través de botones para redes sociales;

* ...y algunos otros detalles pensados especialmente que ¡les invitamos a descubrir!

Nos alegra contarles que el diseño es de nuestre compañere Librenauta, con quien pensamos colaborativamente las funcionalidades disponibles.  Pueden ver más de sus creaciones y proyectos de cultura libre y cyborg en su [sitio](https://copiona.com) y también contribuir a través de su [billetera digital](https://copiona.com/donaunbit/).

## ¿Por qué una plantilla para adhesiones?

Esta es una de las primeras plantillas que quisimos desarrollar. Lo pensamos en función de las necesidades de organizaciones, colectivos, activistas... y con el propósito de ofrecer una herramienta sencilla que pudiera cumplir con este objetivo **sin ceder datos** delicados a compañías largamente probadas de realizar espionaje (Por ejemplo, [Google Forms](https://actualidad.rt.com/actualidad/167003-google-companias-internet-tecnologia-eeuu-espionaje)) o sin tener que recurrir a **plataformas más complejas en otros idiomas**, de las que también podemos [cuestionar](https://www.xataka.com/servicios/change-org-asi-funciona-gran-negocio-activismo-online) su modelo de negocio, formas de promover algunas campañas por sobre otras y venta de datos...

Esta idea tomó fuerza tras nuestra [primera jornada de intercambio](tuvimos-nuestra-primera-jornada-de-intercambio/). Estamos muy felices de que hoy esté disponible para sumar a las luchas.
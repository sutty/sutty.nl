---
title: "¡Lanzamiento! Nueva función de moderación para los sitios conectados al Fediverso"
description: "\U0001F389 ¡Novedades de Sutty &amp; Distributed Press! Potenciamos
  las experiencias de cuidado colectivo en las redes sociales descentralizadas con
  funciones de moderación. Expandí tus horizontes e interactúa aún más en las redes
  digitales, a la vez que permaneces protegide del acoso, los spammers y la hostilidad."
author: []
categories:
- dWeb
- Fediverso
- publicaciones
- anticensura
tags: []
draft: false
activity: true
order: 32
locales: []
layout: post
uuid: f2328929-9c11-44a4-bc6d-545fb6dc47f5
render_with_liquid: false
usuaries:
- 1
- 310
created_at: 2024-04-26 14:55:18.914658928 +00:00
last_modified_at: 2024-06-03 21:45:02.677515677 +00:00
---

<p style="text-align:start">En el proyecto <em><strong>Distributed Press</strong></em> desarrollamos herramientas que facilitan el alojamiento en la web distribuida mediante varios protocolos peer-to-peer para que los sitios sean rápidos, distribuidos y conectados a interacciones sociales. <mark style="color:inherit;background-color:#adffe0">Ideal para aquellas personas que necesitan protección contra la censura y valoran la soberanía de sus datos.</mark></p>
<p style="text-align:start">Hoy añadimos la <strong>Moderación Personalizada ante las interacciones en Redes Sociales Descentralizadas</strong>, para los sitios gestionados a través del CMS Sutty.</p>
<h4 style="text-align:start" id="lo-nuevo:-si-creas-y-gestionas-tus-sitios-con-sutty-cms-ahora-la-integracion-con-el-fediverso-viene-reforzada-con-las-funciones-de-moderacion-de-activitypub-de-distributed-press.">🎉 Lo nuevo: si creás y gestionás tus sitios con Sutty CMS, ahora la integración con el Fediverso viene reforzada con las funciones de moderación de ActivityPub de Distributed Press.</h4>
<figure><img src="public/ieb9rv6kk83arqqiibc0dbt8q0z8/moderacionnn.png" class="img-fluid" controls="false"></figure>
<p style="text-align:start"></p>
<p style="text-align:start">Sutty está disponible de forma <strong>gratuita</strong> para quienes desean <strong>publicar y gestionar sitios web sin programar</strong>:</p>
<ul>
  <li>
    <p style="text-align:start">Podés utilizar una interfaz gráfica intuitiva para escribir publicaciones y personalizar tu sitio, y luego activar la integración con Distributed.Press para que Sutty suba tu sitio a IPFS y Hypercore además de la versión HTTP.</p>
  </li>
  <li>
    <p style="text-align:start">Podés difundir tu contenido e interactuar en las redes digitales: tus artículos pueden ser publicados en las redes sociales del Fediverso, mediante un perfil personalizable en una instancia de Sutty única para tu sitio web. Les usuaries del Fediverso pueden seguir tus publicaciones, mencionarte y responderte.</p>
  </li>
</ul>
<p style="text-align:start">Hoy enriquecemos las funciones de moderación e integración a redes sociales federadas, con:</p>
<p style="text-align:start">✳ Recepción y moderación de comentarios en tu sitio y publicaciones. </p>
<p style="text-align:start">✳ Gestión de listas de permitidos y bloqueados a nivel de instancia. </p>
<p style="text-align:start">✳ Soporte para las listas de bloqueo de Fediblock, bloqueando instancias conocidas por defecto. </p>
<p style="text-align:start">✳ Posibilidad de permitir o bloquear usuaries individuales, para que puedas bloquear una instancia pero permitir que usuaries individuales comenten</p>
<p style="text-align:start">Muchas <strong>interacciones sociales</strong> en Internet se vuelven <strong>desagradables</strong> cuando las personas <strong>nos enfremos a trolls o agentes odiantes.</strong> Estamos buscando <mark style="color:inherit;background-color:#adffe0">estrategias y mecanismos de cuidado colectivo que respalden el Derecho a la Comunicación de las personas</mark>. En las vastas comunidades en línea del Fediverso, es posible regular la circulación de fascismo y discursos de odio, ya que cada instancia puede excluir a otras instancias con informes previos de contenido antidemocrático, odiante o contrario a los valores y al cuidado de sus habitantes. <strong>Expandí tus horizontes e interactúa aún más en las redes digitales, a la vez que permaneces protegide del acoso, los spammers y la hostilidad.</strong></p>
<p style="text-align:start">Comenzá con la plataforma sin código de Sutty. Creá un sitio web estático y utilizá el panel de administración para publicar tu sitio de forma distribuida, integrándote al Fediverso y encontrando a nueves lectores donde estén: <a href="https://sutty.nl/es/" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">https://sutty.nl/es/</a></p>
<p style="text-align:start"></p>
<figure><img src="public/abc8ker5988inu8y6bfd3d1dvva1/footer-espaniol-M3.png" class="img-fluid" controls="false"></figure>
<p style="text-align:start"></p>
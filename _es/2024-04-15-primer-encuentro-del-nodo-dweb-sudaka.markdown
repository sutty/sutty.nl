---
title: Primer Encuentro del Nodo dWeb Sudaka
description: Este sábado 20/4 de 18 a 20hs en el CEDINCI (Rodriguez Peña 356, CABA)
  nos encontramos para activar el nodo local del movimiento por una web distribuida,
  la dWeb. Buscamos formas más justas, sustentables y solidarias de comunicación.
author:
- Sutty
image:
  path: public/x0pltb69s2crkejbnyy3x00oibpk/2024-04-20-flyer.png
  description: Flyer con la información replicada en el artículo
categories: []
tags: []
draft: false
activity: true
order: 31
locales: []
layout: post
uuid: 975bbc05-33e0-4ec6-9cda-dd1bc9c21620
render_with_liquid: false
usuaries:
- 1
created_at: 2024-04-15 15:40:56.797924564 +00:00
last_modified_at: 2024-04-15 15:51:58.240140106 +00:00
---

<p style="text-align:start">Este sábado 20/4 de 18 a 20hs en el <a href="https://cedinci.org/" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">CEDINCI</a> (Rodriguez Peña 356, CABA) nos encontramos para activar el <a href="https://dweb.sutty.nl" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">nodo local del movimiento por una web distribuida</a>, la <a href="https://getdweb.net" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">dWeb</a>. Buscamos formas más justas, sustentables y solidarias de comunicación.</p>
<p style="text-align:start">Sabemos que la concentración de unas pocas corporaciones manejando la Internet nos pone en riesgo. Te invitamos a ser parte de este primer encuentro para conocernos y compartir objetivos de acción.</p>
<p style="text-align:start">Si sos archivista, pirata, jáquer, comunicadore comunitarie, periodista, artista o artivista, tenés un servidor en una computadora reciclada, pertenecés a una red o radio comunitaria, querés que las tecnologías se construyan desde la solidaridad, te esperamos con algo de comida y bebida para compartir. Si necesitás ayuda para el transporte, <a href="https://sutty.nl/#contacto" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">escribinos</a>.</p>
<p style="text-align:start">Convoca <a href="https://sutty.nl" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">Sutty</a>. <a href="https://sutty.nl/codigo-de-convivencia/" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">Código de convivencia</a>.</p>
<h2 style="text-align:start" id="que-es-esto-de-la-dweb">¿Qué es esto de la dweb?</h2>
<p style="text-align:start">La web distribuida es la idea de que no es necesario delegar nuestra información a los grandes proveedores de Internet para poder resguardarla. Por el contrario, que todes podamos participar en la distribución de información, ¡que nosotres seamos la red!</p>
<p style="text-align:start">Esto desarma la dependencia en los grandes distribuidores, de la misma forma que le copiamos un pendrive a une amigue, o digitalizamos un libro para que todes lo puedan leer, o descargamos una película por torrent y la dejamos un rato más para que alguien más también la vea.</p>
<p style="text-align:start">Desde Sutty proponemos que este modelo nos permite desmantelar el monocultivo de servidores y generar redes autónomas que no dependan de empresas que lucran con la vigilancia y la manipulación de información, sino que crezcan y se expandan en redes solidarias.</p>
<p style="text-align:start">Desde el 2022 participamos más activamente de este movimiento y junto con otres compes de latinoamérica comenzamos a reunirnos y discutir. Nos comprometimos con la organización de un nodo local y comenzamos por Buenos Aires, Argentina, con ganas de pensar, sistematizar, elaborar perspectivas y protocolos desde nuestros territorios.</p>
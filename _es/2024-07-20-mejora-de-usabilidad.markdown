---
title: Mejora de usabilidad
description: Les proponemos opinar sobre sus experiencias en el manejo de sitios web,
  tanto si han usado nuestra plataforma como si no la han usado. Encuentren unos breves
  formularios en este artículo, ¡y gracias por contarnos!
author: []
categories: []
tags: []
draft: false
activity: false
order: 33
locales: []
layout: post
uuid: edcbd205-81b9-4f58-8a10-5cf010d575bd
render_with_liquid: false
usuaries:
- 310
created_at: 2024-07-20 13:50:28.263672970 +00:00
last_modified_at: 2024-07-20 14:00:56.178231267 +00:00
---

<h4 style="text-align:start" id="hola-amigues-de-sutty!">¡Hola, amigues de Sutty!<br>
  <br>
  Les contamos que estamos trabajando en la mejora de la usabilidad de nuestra plataforma gratuita (gestora de publicaciones para sitios resilientes).<br>
  <br>
  Pueden ayudarnos mucho contestando unas preguntas que preparamos, éstas son las opciones:</h4>
<h4 style="text-align:start" id="">- <a href="https://cryptpad.fr/form/#/2/form/view/qd2Bv-vAA7yGJd7Ab8PwChz4pq2t8DAJgW+rIU0pPL4/" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">Si ya usan o usaron la plataforma o el cms de Sutty</a>
</h4>
<h4 style="text-align:start">
  <br>
  - <a href="https://cryptpad.fr/form/#/2/form/view/dgR5EviVjY5EktVpVyv+wBNx1SItgm4LdRpYN7O35uY/" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">Si usan Sutty y activaron la función de publicar en el Fediverso:</a>
</h4>
<h3 style="text-align:start"></h3>
<h4 style="text-align:start" id="">- <a href="https://cryptpad.fr/form/#/2/form/view/onZCXPoTd55KIfA3LNhtbvxwSYqDAtJDwqB5rictGKw/" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">Si todavía no usaron Sutty: </a><br>
  <br>
  Si usan el fediverso, nos ayudan además compartiendo <a href="https://todon.nl/@sutty/112644889301585628" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">esta publicación</a><br>
  <br>
  Nos pueden contactar directamente a <a href="mailto:uxdi@sutty.coop.ar" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">uxdi@sutty.coop.ar</a><br>
  <br>
  ¡Les agradecemos y les mandamos un abrazo solidario!</h4>

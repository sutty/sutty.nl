---
title: Sutty visita Hackbo
description: Vamos a estar de visita en Bogotá conversando sobre dWeb
author:
- Sutty
image:
  path: public/vzqfcm91yeci3ska831vwjp0h9ll/dweb.jpg
  description: Flyer
categories: []
tags: []
draft: false
order: 27
locales: []
layout: post
uuid: bdd64ade-add4-4681-ad15-80c5b8f15f84
liquid: false
usuaries:
- 1
last_modified_at: 2023-07-31 23:30:28.239823525 +00:00
---

<p style="text-align:start">Este viernes en Bogotá nos encontramos compas de Sutty, Hackbo y otros proyectos a charlar sobre la web distribuida, infraestructuras colectivas y sitios web resilientes, entre otras. Habrá tigres! Y un recorrido por el bosque del Hackbo. Pueden acercarse el 4 de Agosto, 2023 de 4 a 7 pm a Calle 44 # 8 - 50. Apto. 201. Bogotá, Colombia. <a href="https://hackbo.org/" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">https://hackbo.org/</a></p>
---
title: Sutty y Hypha Coop presentan la herramienta de publicación automática al Fediverso.
  ¡Agitando comunidades!
description: "¡Te invitamos a conocer acerca de nuestro proyecto colectivo de publicaciones
  distribuidas para comunidades!"
author:
- Sutty
image:
  path: public/aum54z5l5z13wdyxdo55dd0yn819/https __cdn.evbuc.com_images_632248079_303135443722_1_original.jpg
  description: Imagen de la actividad Weaving Communities for the Digital Commons.
    Un planeta tierra envuelto en flores
categories:
- DWeb
- Fediverso
tags:
- eventos
- presentaciones
- comunidad
- Dweb
- publicaciones
- distribuido
- Fediverso
- Distributed Press
- Hypha Coop
draft: false
order: 29
locales: []
layout: post
uuid: 6be82590-1af5-487b-aa9d-74a17814118f
render_with_liquid: false
usuaries:
- 5
created_at: 2023-11-02 16:23:25.954667156 +00:00
last_modified_at: 2023-11-02 16:36:59.460282442 +00:00
---

<p style="text-align:start">¿Te suena el Fediverso? ¿Alguna vez oíste hablar de la <a href="https://sutty.nl/distribuyase/" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">Web Distribuida (DWeb)</a>? ¿Te interesan las tecnologías para fortalecer comunidades?</p><p style="text-align:start">Te esperamos HOY a las 14 (Argentina) en el evento virtual que compartiremos con otres compes. Aquí para anotarte: <a href="https://www.eventbrite.com/e/dweb-virtual-meetup-weaving-communities-for-the-digital-commons-tickets-740487075777?aff=oddtdtcreator" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">https://www.eventbrite.com/e/dweb-virtual-meetup-weaving-communities-for-the-digital-commons-tickets-740487075777?aff=oddtdtcreator</a></p><p style="text-align:start">Aunque el evento de hoy es en inglés, estamos organizando nodos locales, si te interesa participar, ¡escribinos! :D</p><h2 style="text-align:start" id="mas-info">Más info</h2><p style="text-align:start">En el marco del <a href="https://www.eventbrite.com/e/dweb-virtual-meetup-weaving-communities-for-the-digital-commons-tickets-740487075777?aff=oddtdtcreator" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">Encuentro Virtual de la Dweb (Web Distribuida) Agitando comunidades para les comunes digitales</a>, fauno nuestro compa de la coope Sutty y nuestre compe Mauve de <a href="https://hypha.coop/" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">Hypha Coop</a> presentarán la herramienta de publicación automática desde los sitios de Sutty al <a href="https://es.wikipedia.org/wiki/Fediverso" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">Fediverso</a>. Venimos uniendo esfuerzos desde hace dos años en generar herramientas que faciliten la publicación en la Dweb junto con <a href="https://distributed.press/" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">Distributed Press</a>, <a href="https://hypha.coop/" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">Hypha Coop</a> y <a href="https://three.compost.digital/" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">COMPOST Magazine</a>, con el apoyo de la <a href="https://www.ffdweb.org/" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">Filecoin Foundation for the Distributed Web</a>. El evento es apto para todo público :) (Aunque puede ser un poco techie)</p><p style="text-align:start">El evento será facilitado por le compañere Mai Ishikawa Sutton, tendrá una duración de dos horas y contará con las presentaciones de otres compañeres y colectivos trabajando por la DWeb.</p>
---
title: Presentación de Sutty
description: Nace una estrella
author:
- Sutty
categories:
- Sutty
tags: []
order: 0
locales: []
layout: post
uuid: fa3c490f-b596-40df-9aa1-51e640834e4f
liquid: false
usuaries: []
last_modified_at: 2021-06-27 17:33:37.132520057 +00:00
---

<h2>Introducción</h2>
<p>Cuando queremos tener nuestro propio sitio solemos recurrir a gestores
de contenido, como Wordpress y Drupal, dos de los gestores libres más
utilizados.  El gestor de contenido es el software que nos permite
editar el contenido del sitio, su estructura y diseño.  Por cada visita,
toman estas distintas partes y las combinan para generar una página web.</p>
<p>Sin embargo, para lograrlo requieren de varias piezas de software, como
bases de datos, servidores web e intérpretes de lenguaje, que deben
funcionar correctamente durante cada visita.  Esto los hace dinámicos.
Un pequeño cambio en alguna de las partes provoca una actualización
inmediata en todo el sitio.  Pero si una parte falla, el sitio deja de
estar disponible.</p>
<p>Además, deben mantenerse actualizados, ya que al ser software
funcionando en una computadora, una vulnerabilidad puede permitir a los
atacantes acceso al sitio, tanto como para extraer información como para
modificarla.  Las vulnerabilidades se resuelven con actualizaciones
constantes, aunque no todas las vulnerabilidades son solucionadas o
solucionables.</p>
<p>El correr en una computadora generando páginas web a demanda utiliza
recursos, como capacidad de procesamiento o utilización de memoria.
Esto implica que a más visitas hay cada vez más costos monetarios y/o
partes móviles como cachés, por un lado, pero también y de forma
invisible, un costo ecológico cada vez más alto, debido a que el uso de
recursos informáticos redunda en consumo de energía y emisión de calor.</p>
<p>Decimos que todo este proceso es invisible porque todos estos procesos
se dan en computadoras, pero de otras entidades.  Estas computadoras
invisibles se encuentran apiladas en galpones gigantescos, enfriadas
constantemente con aires acondicionados.  Es una práctica común que las
partes de estas computadoras se cambien no cuando fallan, sino cuando su
garantía vence, aun cuando no están demostrando fallas físicas.</p>
<p>Todo esto contribuye a una escalada de explotación humana, consumo
energético, extracción de recursos, basura electrónica y obsolecencia
programada.</p>
<h2>Todas las vulnerabilidades son políticas</h2>
<p>Las vulnerabilidades, los cuellos de botella, el acceso a mayores
recursos o menos recursos tienen usos políticos que nos exponen
colectivamente.  Habilitan, aun sin quererlo, violencias, censura e
invisibilización en la forma de ataques de <em>defacing</em><sup><a href="#fn1" id="fnref1">1</a></sup>,
DDOS<sup><a href="#fn2" id="fnref2">2</a></sup>, <em>doxxing</em><sup><a href="#fn3" id="fnref3">3</a></sup> y otros.</p>
<p>Creemos en un ciberespacio libre de violencias y eso implica que lo
podamos habitar sin ser acosadxs ni acalladxs.  Las vulnerabilidades de
software redundan muchas veces en violencias hacia nosotrxs.</p>
<h2>Sitios estáticos, seguros, ecológicos y justos</h2>
<p>Los sitios dinámicos no son la única posibilidad que tenemos para
publicar sitios web de forma más segura y ecológica.  Los sitios
estáticos también son capaces de mezclar contenido, estructura y diseño,
pero a diferencia de los dinámicos, este proceso se hace por única vez.
En lugar de generar una página por cada visita, todas las páginas que
componen el sitio se generan de una sola vez, ahorrando el consumo de
recursos.  Si hay un cambio, el sitio se vuelve a generar.  Aunque
parezca extraño, este proceso suele tomar algunos segundos.</p>
<p>Las ventajas de esta estrategia técnica son que el sitio se independiza
del software y puede funcionar en cualquier momento y en cualquier
lugar.  Los sitios estáticos también son transportables.  Al no
necesitar software para ser visualizado, son invulnerables a la mayoría
de los ataques más comunes a sitios web<sup><a href="#fn4" id="fnref4">4</a></sup> y son más
veloces, porque el trabajo ya se hizo una vez.</p>
<p>Además, los consideramos más ecológicos y justos porque al consumir
menos recursos, contribuyen mucho menos a la explotación de recursos
naturales y de trabajadorxs, principalmente en África y Asia.  Incluso
hay colectivxs alojando <a href="https://solar.lowtechmagazine.com/2018/09/how-to-build-a-lowtech-website/">sus sitios en microservidores alimentados por
paneles
solares</a>.</p>
<p>Otra ventaja es que los sitios estáticos no suelen ocultar las
complejidades técnicas detrás de interfaces bonitas y pretendidamente
más simples de utilizar.  Nos empoderan como usuarixs obligándonos a
tomar contacto con la tecnología en sí y aprendemos de HTML, CSS,
lenguajes de programación, librerías y largos etc., a través de
herramientas simples en lugar de infraestructuras complejas.</p>
<p>Entendemos, claro, que esto último no puede ser deseable para todo el
mundo y por eso proponemos Sutty como la intersección entre sitios
estáticos seguros, justos, ecológicos y la inmediatez de uso de los
gestores de contenido más hegemónicos.</p>
<h2>Sutty</h2>
<p>Sutty es una gestora de contenidos que aprovecha la madurez
técnica de uno de los generadores de sitios estáticos más utilizados,
llamado Jekyll.</p>
<p>La llamamos Sutty porque queríamos jugar con el costado literario de
Jekyll y a la vez recuperar y visibilizar otras perspectivas y otros
cuerpos.  Sutty es, pues, el nombre de una antropóloga interestelar,
lesbiana e india, del universo de ciencia ficción narrado por Ursula K.
Le Guin.</p>
<p>En cuanto a lo técnico, Sutty permite editar contenidos y publicarlos a
través de Jekyll.  Al ser dos proyectos corriendo en paralelo, es
posible modificar el sitio a través ambos a la vez, e incluso
independizarlo de Sutty.  Creemos que esto permite que nos apropiemos de
la propuesta de los sitios estáticos, sin ocultar complejidades ni
desempoderarnos como usuarixs.</p>
<p>Nuestra apuesta es reducir la dependencia en infraestructuras ajenas y
promover una web más justa y distribuida.  Por eso, cuando se genera un
sitio a través de Sutty, podrá ser publicado en cualquier plataforma.</p>
<p>Nuestra apuesta es ética y política.  Queremos que Sutty sea una
herramienta de difusión para nuestrxs colectivxs y campañas.  No se
trata de un servicio en la nube que se pretende políticamente neutro, al
mismo tiempo que habilita el auge del fascismo.</p>

<ol>
<li id="fn1">
<p>Es decir, ingresar a un sitio y cambiarlo para que diga
otra cosa. <a href="#fnref1">↩</a></p>
</li>
<li id="fn2">
<p>Los (D)DOS o denegación (distribuida) de servicio son ataques
que apuntan a agotar los recursos de un sitio ya sea por su masividad
o por amplificación, insumiendo un costo mucho menor a los atacantes. <a href="#fnref2">↩</a></p>
</li>
<li id="fn3">
<p>El <em>doxxing</em> es un ataque de amedrentamiento más social que
técnico e implica la publicación de datos privados como dirección,
números de teléfono, nombres de familiares, etc. con el objetivo de
incitar a otros a participar del acoso. <a href="#fnref3">↩</a></p>
</li>
<li id="fn4">
<p>No estamos contando otras vulnerabilidades
posibles, como las de la computadora en sí, aunque generalmente más
dificultosas. <a href="#fnref4">↩</a></p>
</li>
</ol>
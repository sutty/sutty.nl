---
title: Lanzamiento de publicaciones distribuidas en el Fediverso a través de Sutty
description: "¿Compartir contenido a través de redes sociales libres y descentralizadas
  automáticamente desde mi sitio web? ¡Guau!"
author:
- Sutty
image:
  path: public/8r7b6ohqy6xzgngxbol6337q8jj9/milestone_2_activity_pub_2.png
  description: Botones de colores para activar la "Web Disribuida" y el "Fediverso".
categories:
- dWeb
- Fediverso
- publicaciones
- anticensura
- redes sociales
tags:
- dWeb
- Fediverso
- Pixelfed
- Mastodon
- Lanzamiento
- Distributed Press
- distribuido
- Hypha Coop
draft: false
activity: true
in_reply_to: https://hypha.coop/dripline/announcing-dp-social-inbox/
order: 30
locales:
- db7b6085-dcae-4d2f-9c98-8aee65b8bfb7
layout: post
uuid: fb05c2ff-f92c-4652-b640-09373b34f15f
render_with_liquid: false
usuaries:
- 5
created_at: 2023-12-04 21:53:05.206371985 +00:00
last_modified_at: 2024-06-19 13:40:51.844146001 +00:00
---

<p style="text-align:start">Estamos felices y orgulloses de anunciar el lanzamiento de la funcionalidad que permite la publicación en el Fediverso de los artículos de todos los sitios creados a través de Sutty.</p>
<p style="text-align:start">Gracias al trabajo conjunto con <a href="https://distributed.press/" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">Distributed Press</a>, <a href="https://hypha.coop/" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">Hypha</a> y apoyado por la <a href="https://www.ffdweb.org/" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">Filecoin Foundation for the Distributed Web</a>, Sutty hace posible que la seguridad de tu sitio estático se combine con la rápida difusión de tu contenido a través de las redes sociales libres y descentralizadas que constituyen el Fediverso.</p>
<p style="text-align:start">Esto se logró a través del desarollo y la integración de dos componentes, trabajados en forma conjunta y colaborativa:</p>
<ol>
  <li>
    <p style="text-align:start"><a href="https://github.com/hyphacoop/social.distributed.press" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">Social Inbox</a>, desarrollado principalmente Distributed Press. Aporta la funcionalidad de recibir artículos, responder y mencionar otras cuentas en el Fediverso.</p>
  </li>
  <li>
    <p style="text-align:start"><a href="https://0xacab.org/sutty/jekyll/jekyll-activity-pub" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">Jekyll Activity Pub Plugin</a>, desarrollado principalmente por Sutty. Permite integrar Social Inbox en todos los sitios estáticos generados en Jekyll, admitiendo así la publicación automática de contenido del sitio en el Fediverso.</p>
  </li>
</ol>
<p style="text-align:start"><a href="https://sutty.nl" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">Sutty</a> integra la funcionalidad completa en su CMS para sitios estáticos en Jekyll, permitiendo gestionarla desde una interfaz en continua mejora de su usabilidad.</p>
<p style="text-align:start">Si todavía no estás familiarizade con estos nombres y conceptos, te invitamos a conocer más a continuación, en la sección “Para tecno-curioses”.</p>
<h3 style="text-align:start" id="que-significa">Qué significa</h3>
<ul>
  <li>
    <p style="text-align:start">Que si creás tu sitio web a través de Sutty, tenés nuevas posibilidades de difundir tus contenidos e interactuar en redes digitales.</p>
  </li>
  <li>
    <p style="text-align:start">Que tus artículos pueden ser publicados en las redes del Fediverso.</p>
  </li>
  <li>
    <p style="text-align:start">Que tu sitio tendrá un perfil o usuarie personalizable desde el panel en una instancia de Sutty propia.</p>
  </li>
  <li>
    <p style="text-align:start">Que les usuaries del Fediverso pueden seguir tus publicaciones.</p>
  </li>
  <li>
    <p style="text-align:start">Que les usuaries del Fediverso que te sigan podrán leer tus publicaciones, mencionarte y responderte.</p>
  </li>
  <li>
    <p style="text-align:start">Que podrás interactuar con les usuaries del Fediverse con las opciones de responderles y mencionarles.</p>
  </li>
</ul>
<h3 style="text-align:start" id="que-permite-hacer">Qué permite hacer</h3>
<ul>
  <li>
    <p style="text-align:start">Activar la publicación en el Fediverso para todos los sitios de Sutty.</p>
  </li>
  <li>
    <p style="text-align:start">Activar la publicación de los artículos que quieras en el Fediverso.</p>
  </li>
  <li>
    <p style="text-align:start">Responder comentarios desde los artículos de tu sitio en Sutty.</p>
  </li>
  <li>
    <p style="text-align:start">Personalizar la cuenta que Sutty crea automáticamente de tu sitio en el Fediverso.</p>
  </li>
  <li>
    <p style="text-align:start">Reportar o informar de usuaries o instancias abusivos mediante nuestro <a href="https://sutty.nl/index.html#contacto" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">formulario de contacto</a>.</p>
  </li>
</ul>
<h3 style="text-align:start" id="que-se-viene">Qué se viene</h3>
<ul>
  <li>
    <p style="text-align:start">Mejoras en la integración de las respuestas como comentarios en el sitio.</p>
  </li>
  <li>
    <p style="text-align:start">Incorporación de <em>menciones</em> desde el panel de Sutty.</p>
  </li>
  <li>
    <p style="text-align:start">Mejoras en la interfaz general del panel.</p>
  </li>
  <li>
    <p style="text-align:start">Nuevas implementaciones para una mejor moderación.</p>
  </li>
  <li>
    <p style="text-align:start">Mejor compatibilidad con diversas redes en el Fediverso (Mastodon+Glitch, Pleroma, Ktistec).</p>
  </li>
  <li>
    <p style="text-align:start">Mejoras que permitirán diferenciar el contenido a publicar en el Fediverso y en el sitio de Sutty.</p>
  </li>
  <li>
    <p style="text-align:start">La posibilidad de exportar tu cuenta a una instancia del Fediverso desde tu panel.</p>
  </li>
  <li>
    <p style="text-align:start">La posibilidad de que Sutty anuncie tu contenido y/o usuarie del Fediverse en forma automática para atraer seguidorxs. (Ahora, podés hacerlo <a href="https://cryptpad.fr/form/#/2/form/view/XorL4I-nC17rcEwtol3ghsRDsivfg6g5685MK+TFZ-8/" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">a través de un formulario</a>).</p>
  </li>
  <li>
    <p style="text-align:start">Acceder a la lista de seguidorxs y seguides desde tu panel.</p>
  </li>
  <li>
    <p style="text-align:start">Seguir, dejar de seguir, bloquear usuaries y/o instancias del Fediverso desde el panel.</p>
  </li>
</ul>
<h3 style="text-align:start" id="quiero-usarlo!">¡Quiero usarlo!</h3>
<p style="text-align:start">Te invitamos a dar tus primeros pasos de la mano de <a href="https://dweb.sutty.nl/2023/11/29/tutorial-de-publicaci%C3%B3n-en-el-fediverso-desde-tu-sitio.html" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">nuestro tutorial</a>.</p>
<h3 style="text-align:start" id="para-tecno-curioses">Para tecno-curioses</h3>
<h3 style="text-align:start" id="como-funciona">Cómo funciona</h3>
<p style="text-align:start">Los sitios web y las redes sociales parecen ser especies distintas dentro del Universo de Internet. Al mismo tiempo, las redes sociales corporativas y concentradas como Instagram, Facebook, X (ex Twitter), entre otras, demostraron ser hostiles con algunos grupos o colectivos sociales en particular (censurando contenido, persiguiendo pezones, ocultando publicaciones por color de piel y de pelo, etc.) y con todes sus usuaries en general (vendiendo data en forma masiva, violando acuerdos de privacidad, eligiendo diseños de interfaz y uso que generan ansiedad y adicción, etc.). Pese a esto, siguen funcionando como espacios obligados a la hora de publicitar un emprendimiento o difundir noticias urgentes.</p>
<p style="text-align:start">El Fediverso es una red federada, descentralizada y distribuida de redes sociales libres, cada una con sus características, preferencias, grupos de usuaries. Están diseñadas para facilitar el diálogo entre todas ellas. Es decir, para que los contenidos puedan ser visibles y se puedan generar respuestas entre usuaries, fomentando una cultura de participación y pluralidad de voces, basadas en estándares de desarrollo libre y que buscan ser éticos antes que con fines de lucro sin fin.</p>
<p style="text-align:start">Los sitios web siguen siendo formatos para medios de comunicación que, debido a sus características, favorecen la difusión de contenidos como artículos multimedia. Permiten adecuar un estilo a una identidad visual del medio, mantener secciones y contenido institucional variado, entre otras cosas.</p>
<p style="text-align:start">Las redes sociales se destacan por sus características de <em>inmediatez</em>, favoreciendo un flujo dialógico <em>en tiempo real</em> con otros tiempos de atención y características de navegación que lo hacen más breve, rápido, a veces efímero. Los medios de comunicación (personas o emprendimientos mediáticos) suelen utilizarlos para llamar la atención sobre contenidos publicados en sus sitios, apostando a la divulgación rápida y las discusiones que puedan darse entre usuaries.</p>
<p style="text-align:start">La funcionalidad que desarrollamos en Sutty contempla los casos de uso en los que un contenido quiera ser compartido a más personas, en menor tiempo, con la posibilidad de generar diálogos. Las particularidades de nuestros sitios y redes sociales libres generan condiciones favorables para la libertad de expresión, que preferimos llamar Derecho a la Comunicación, evadiendo las variadas y cada vez más sofisticadas formas de censura de las plataformas corporativas tradicionales. Un contenido reproducido en varios lugares al mismo tiempo ayuda a su divulgación y es ideal para aquellas voces y discursos contrahegemónicos en la web y su supervivencia al paso del tiempo, preservando la memoria popular.</p>
<h3 style="text-align:start" id="como-funciona-el-fediverso-en-la-moderacion">Cómo funciona el Fediverso en la moderación</h3>
<p style="text-align:start">El Fediverso intenta funcionar como comunidades en línea interconectadas que se autogobiernan en las formas de cuidados colectivos. Así, cada instancia podría ser algo así como un municipio que aloja diferentes cuentas/usuaries bajo unas reglas consensuadas y que pueden ser puestas en discusión si fuera necesario. De esta forma, es posible regular la circulación de contenidos fascistas y discursos de odio que puedan dañar no solamente la participación de diverses usuaries sino también su salud.</p>
<p style="text-align:start">Para ello, cada instancia elige sus formas de moderación y puede excluir otras instancias con denuncias previas de contenidos antidemocráticos, odiantes o contrarios a los valores y cuidados de sus habitantes.</p>
<p style="text-align:start">En Sutty en particular, nos interesan las estrategias y los mecanismos de cuidados colectivos, por lo que seguimos diseñando modelos que permitan sostenerlos en nuestras tecnologías. Podés revisar nuestros <a href="https://sutty.nl/terminos-de-servicio/" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">términos y condiciones</a>, <a href="https://sutty.nl/politica-de-privacidad/" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">política de privacidad</a> y <a href="https://sutty.nl/codigo-de-convivencia/" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">acuerdos de convivencia</a> para más información.</p>
<h3 style="text-align:start" id="te-interesa-participar">¿Te interesa participar?</h3>
<p style="text-align:start">Si sos parte de una organización social, grupo de activismo o colectivo social que pensás que podría beneficiarse de estas características, te invitamos a <a href="https://sutty.nl/index.html#contacto" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">contactarnos a través de nuestro formulario</a>. Estamos buscando mejorar los usos de las tecnologías para ustedes y valoramos sus experiencias.</p>
<h3 style="text-align:start" id="otras-posibilidades-de-integracion-de-social-inbox-en-sitios-estaticos">Otras posibilidades de integración de Social Inbox en sitios estáticos</h3>
<p style="text-align:start">Si te interesa incorporar esta funcionalidad para otros gestores de sitios estáticos, no dudes en <a href="https://sutty.nl/index.html#contacto" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">contactarnos</a>. Además, mantenete al tanto de las novedades que compartimos en <a href="https://dweb.sutty.nl" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">https://dweb.sutty.nl</a> y en nuestro blog <a href="https://sutty.nl/blog" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">https://sutty.nl/blog</a></p>
<h3 style="text-align:start" id="recomendado-para-saber-mas">Recomendado para saber más</h3>
<ul>
  <li>
    <p style="text-align:start">Presentación en MeetUp (en inglés): <a href="https://hackmd.io/3xsFFy-RSwWJnPjFKUco1w?view#/1" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">https://hackmd.io/3xsFFy-RSwWJnPjFKUco1w?view#/1</a></p>
  </li>
  <li>
    <p style="text-align:start">Blog en el que recopilamos info sobre la web distribuida para compartir en castellano: <a href="https://dweb.sutty.nl" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">https://dweb.sutty.nl</a></p>
  </li>
  <li>
    <p style="text-align:start">Nuestro blog general: <a href="https://sutty.nl" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">https://sutty.nl</a></p>
  </li>
  <li>
    <p style="text-align:start">Blog de Jekyll Activity Pub Plugin: <a href="https://jekyll-activity-pub.sutty.nl/" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">https://jekyll-activity-pub.sutty.nl/</a></p>
  </li>
  <li>
    <p style="text-align:start">Blog de Mauve y su nota sobre Social Inbox (en inglés): <a href="https://blog.mauve.moe/posts/distributed-press-social-inbox" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">https://blog.mauve.moe/posts/distributed-press-social-inbox</a></p>
  </li>
</ul>
---
title: Hackers reparando
description: Les contamos sobre un análisis de nuestro sistema realizado desde India,
  descubriendo algunos puntos a reforzar para ser invulnerables a ataques maliciosos.
author: []
categories: []
tags: []
draft: false
activity: true
order: 35
locales:
- '04834ba5-3a13-44a9-ae89-278292e2cba1'
layout: post
uuid: 20e85b98-d63c-4bfc-a14a-29d20b230e06
render_with_liquid: false
usuaries:
- 310
created_at: 2024-12-03 14:52:09.871565209 +00:00
last_modified_at: 2024-12-11 19:25:58.095158595 +00:00
---

<h5 style="text-align:start" id="en-el-mundo-del-software-libre-pasan-cosas-tan-lindas-y-extranas-que-corresponde-venir-a-contarlo.">En el mundo del software libre, pasan cosas tan lindas y extrañas que corresponde venir a contarlo. <strong>Un mundo donde recibís un email de un hacker, y van a ser buenas noticias.</strong>
</h5>
<p style="text-align:start">Así fue:</p>
<p style="text-align:start">Nos escribió <a href="https://scriptjacker.in/" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">Parth Narula</a> desde India a principios de septiembre, pidiéndonos permiso para hacer un análisis de <a href="https://panel.sutty.nl" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">nuestra plataforma</a>, el cual dimos. Y encontró dos vulnerabilidades que podríamos mejorar. En ambos casos, no se trata de problemas de seguridad o de performance que perjudiquen a nuestres usuaries debido a acciones nuestras, sino que son puntos de nuestro sistema que un agente malicioso podría mal-utilizar para realizar ataques como inundar sus casillas con correos, o invitar a une usuarie para que descargue malware.</p>
<p style="text-align:start">Porque <mark style="color:inherit;background-color:#7cfde3">en la búsqueda de ofrecer software resiliente y seguro</mark>, también tenemos que cuidarnos y <mark style="color:inherit;background-color:#7cfde3">anticipar todos los distintos caminos que los agentes maliciosos podrían crear</mark> para causar un daño y buscar compensación. En nuestro ámbito, un poco de pensamiento paranoico es más que útil ;)</p>
<p style="text-align:start">De modo que el análisis de Parth y sus recomendaciones nos llevaron a <strong>revisar y resolver</strong> rápidamente tres puntos de nuestro panel del CMS para impedir que sean vulnerables a un ataque malicioso.</p>
<p style="text-align:start">1. Mejoramos la protección contra "fuerza bruta", que evita un tipo de ataque donde nos prueben millones de contraseñas, pero a la vez permitiendo que les usuaries hagan un uso normal del inicio de sesión, puedan equivocarse al escribirla y re-intentar.</p>
<p style="text-align:start">2. Mejoramos la protección contra el envío de confirmaciones de correo y de desbloqueo de contraseña, impidiendo un tipo de ataque que se hace al pedir este proceso muchísimas veces e inundando la casilla de un correo no solicitado.</p>
<p style="text-align:start">3. Limitamos la incorporación de vínculos en un espacio no necesario de nuestros sitios, que permitirían un ataque de envío por correo de hipervínculos maliciosos a personas desprevenidas, potencialmente convirtiéndonos en una plataforma para hacer _phishing_.</p>
<p style="text-align:start">Específicamente el <strong>panel del CMS o gestora de contenidos</strong> Sutty es nuestra pieza más delicada en relación a la seguridad, porque es la única parte <strong>dinámica</strong>. Esto significa, en criollo, que <strong>genera contenido sobre la marcha</strong>, dependiendo de bases de datos y de procedimientos en el servidor: procesos complejos que pueden ser explotados si no están bien protegidos.</p>
<p style="text-align:start">Su seguridad requiere medidas adicionales como controles de acceso, validación de entradas y monitoreo continuo. Por ésto, todos los sitios web que hacemos son, en cambio, "estáticos" o "sólidos", igual de interactivos y bellos que cualquier otro pero con una solución técnica que simplifica cómo se generan en el servidor y reducen drásticamente vulnerabilidades a los ataques más comunes de la Internet, como la infección automatizada con <em>malware</em>, <em>publicidad</em>, <em>webshellz</em> e incluso, como utilizan menos recursos, aumentando el costo de hacer ataque de denegación de servicio (DOS).</p>
<p style="text-align:start">Otras medidas que conforman nuestro enfoque de cibercuidados es tener varios servidores en funcionamiento, ser muy obsesives con no solicitar ni almacenar información personal y hacer copias de seguridad cifradas y remotas de todo. Lo que hacemos en nuestra infraestructura está público acá: <a href="https://0xacab.org/sutty/ansible" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">https://0xacab.org/sutty/ansible</a> y acá <a href="https://0xacab.org/sutty/ansible" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">https://gitea.nulo.in/Sutty/-/packages</a></p>
<p style="text-align:start">Además, todos los cambios que les usuaries de Sutty realizan en sus sitios, quedan registrados de forma que se pueden deshacer. Esto quiere decir que si alguien gana acceso a la gestora y realiza cambios indebidos, podemos ayudarte a deshacerlo. Un acceso indebido puede darse ya sea por cuestiones tan simples como dejar abierta la sesión en una computadora compartida o más complejas, como un ataque que aproveche una vulnerabilidad de la plataforma o use técnicas de ingeniería social (o sea, no es un tema técnico sino un engaño para que una persona entregue las llaves de acceso creyendo que se las deja a otra de confianza).</p>
<h5 style="text-align:start" id="gracias-al-software-libre-y-a-las-actividades-de-hackers-eticxs-o-hacktivistas-las-herramientas-digitales-que-usamos-pueden-ser-dia-a-dia-mas-agiles-mas-seguras-mas-soberanas-y-resilientes.">Gracias al software libre y a las actividades de hackers éticxs o hacktivistas, las herramientas digitales que usamos pueden ser día a día más ágiles, más seguras, más soberanas y resilientes. <strong>Le agradecemos mucho a Parth su aporte constructivo y bien intencionado.</strong>
</h5>
<h5 style="text-align:start" id="parth-narula-es">Parth Narula es <em>founder</em> de <em>ScriptJacker</em>, un equipo dedicado a ciberseguridad que hace investigación, divulgación y asesoramiento sobre OWASP, <em>red teaming</em>, hacking ético y <em>pentesting</em>. Comparte recursos, estudios de caso y material de aprendizaje en <a href="https://scriptjacker.in/" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">su sitio</a>. También pueden contactarle por <a href="https://x.com/scriptjacker" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">X</a>, <a href="https://www.linkedin.com/company/scriptjacker/" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">Linkedin </a>y <a href="https://github.com/scriptjacker" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">Github</a>. ¡Salud!</h5>
---
title: Publicación distribuida a través de Sutty
description: Tecnología popular más resiliente con Web Distribuida. ¡Nuestra nueva
  funcionalidad ya está disponible gratuitamente para sus sitios!
author: []
image:
  path: public/69xc07u7vcmd0u8y3cksd1w8jcjp/post12.png
  description: Tecnología popular más resiliente con Web Distribuida.
categories: []
tags: []
draft: false
order: 25
locales:
- d0afe144-e46a-4560-9e9e-492b0f03d23a
layout: post
uuid: cc9fc862-bc29-45da-b427-6909ae14611f
liquid: false
usuaries:
- 1
- 310
- 417
last_modified_at: 2023-04-26 20:35:00.390688181 +00:00
---

<h4 style="text-align:start" id="ayudanos-a-crear-la-nueva-web-que-construye-su-poder-a-traves-de-la-cooperacion"><strong>Ayudanos a crear la nueva web, que construye su poder a través de la cooperación</strong></h4><h4 style="text-align:start"></h4><p style="text-align:start">Lo que hacía a Internet increíble en sus inicios no eran los videos de gatitos arrojando cosas de una mesa. Todo el contenido que hoy nos entretiene, asusta o enoja hoy, surge de un avance técnico que volvió posible algo impensado: que cada persona / dispositivo se vinculara directamente con otros, para pasarle o descargarse archivos.</p><p style="text-align:start">De ese punto de inflexión en 1969 que logró conectar los cuatro primeros nodos universitarios a nuestros días, se perdieron algunas cosas importantes. La mayoría de la arquitectura de esta enorme red global tiene cuellos de botella en unos pocos lugares: las estructuras se volvieron centralizadas. Enviamos nuestra información allí y así les entregamos el control de nuestros datos, de nuestra presencia online o nuestros ingresos económicos.</p><p style="text-align:start">Si sos une activista, periodista o artista, seguramente tengas el hábito de buscar innovación y zonas de transformación permanentemente. Una mezcla de curiosidad y necesidad, para saber dónde se están creando espacios realmente nuevos y relevantes en el tejido social. La web distribuida es un movimiento así. Tiene la misión de re-crear las prácticas peer-to-peer que hicieron de Internet una verdadera revolución socio-técnica. Volver a una distribución colectiva de la información, salir de las autopistas comerciales que nos capturan.</p><p style="text-align:start">¿Perdiste presencia online por links que se rompen o dominios que se pierden?</p><p style="text-align:start">La web distribuida busca resolver éso.</p><p style="text-align:start">¿Tenés material destinado a zonas de muy baja conectividad?</p><p style="text-align:start">La web distribuida puede mejorar su disponibilidad.</p><p style="text-align:start">¿Buscas usar tecnologías innovadoras, que se diseñen pensando en el derecho a la comunicación y la resiliencia?</p><p style="text-align:start">Es aquí.<br></p><p style="text-align:start">Sutty se integra con DistributedPress para facilitar la publicación en la web distribuida, gracias al generoso apoyo de la FFDW. Haz un sitio que puede navegarse en <em>https:</em> pero también a través de protocolos peer-to-peer, Inter-Planetary File System (IPFS), Hypercore y vía BitTorrent. Crea contenidos más resistentes y disponibles sin conexión.&nbsp;<a href="https://sutty.nl/saber-mas-sobre-publicar-a-la-web-distribuida/" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">&nbsp;Aprende más</a> sobre esta funcionalidad Distributed.Press</p><p style="text-align:start">Registrate gratis hoy y participa en la creación de una nueva web, que construye su poder a través de la cooperación. Empieza aquí: <a href="https://sutty.nl" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">sutty</a><a href="http://sutty.nl" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">.nl</a></p>
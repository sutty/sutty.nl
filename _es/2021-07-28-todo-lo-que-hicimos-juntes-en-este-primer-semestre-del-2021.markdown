---
title: Todo lo que hicimos juntes en este primer semestre del 2021
description: Informe semestral de Sutty 2021.
author:
- Sutty
image:
  path: public/lh51z7asojbjbsz4hr98kkjppowx/cifras-reporte3.png
  description: 19 colaboraciones a proyectos de software libre que utilizan jekill.
    38 plugins/complementos que resuelven o mejoran asuntos específicos de jekill.
    Nuestro aporte al Software Libre/ 1er semestre 2021
categories: []
tags: []
order: 22
locales: []
layout: post
uuid: 72b88e44-a2e2-4ab0-8f88-8ab5501d25e4
liquid: false
usuaries:
- 5
- 356
last_modified_at: 2021-07-28 02:17:51.231639794 +00:00
---

<p>

Somos una <strong>cooperativa de trabajo en software libre</strong>, con la misión de facilitar a las <strong>organizaciones sociales</strong>
 el acceso a tecnologías por fuera de las plataformas hegemónicas del 
capitalismo digital. Nuestro servicio es una gestora gratuita para crear
 y gestionar sitios web estáticos que, de otra manera, requieren cierto 
grado de conocimiento técnico. Les usuaries de Sutty <a href="https://www.sutty.nl/">https://www.sutty.nl/</a> cuentan con sitios web <strong>seguros</strong>, robustos, <strong>ecológicos</strong>, libres y <strong>resilientes</strong>,
 y especialmente protegidos frente a ataques. Sus sitios pueden incluir 
una tienda para comercio electrónico, que ofrecemos en modalidad <strong>ayuda mutua</strong><sup><a href="https://docutopia.tupale.co/#fn1" id="fnref1">[1]</a></sup>.
 Por eso sabemos que nuestro trabajo potencia la presencia, la seguridad
 y la libertad de expresión a organizaciones y colectivas activistas.</p><p><strong>Proyectos</strong>:
Además de ofrecer de forma gratuita la gestora sitios web para quienes lo necesiten, realizamos <strong>desarrollos a medida</strong>, con <strong>maquetación</strong> y <strong>funcionalidades específicas</strong> para diversos proyectos. Esto incluye <strong>migrar</strong> desde otras tecnologías, como Wordpress, con diseño y contenido hacia la gestión de Sutty, <strong>implementar diseños</strong> realizados por la Cooperativa de Diseño, trabajar en <strong>sitios estáticos</strong> en general y en <strong>webapps</strong>.</p><p>En
 este semestre estuvimos trabajando y acompañando un montón de 
proyectos que iremos anunciando en nuestras redes en la medida que 
lancen y consientan su difusión.</p><p><strong>El equipo:</strong>
Crecimos en equipo, ya somos <strong>10</strong> socies y colaboradores,
 incluyendo (h)a(ck)ctivistas del software y la cultura libre, 
infraestructuras autónomas y también feminismos, trans, lgbttiqnb, 
gremiales, comunicadorxs, educadorxs…</p><p>En
 mayo 2021 comenzamos el trámite para constituirnos como cooperativa, y 
nos hicimos miembres de Federación Argentina de Cooperativas de Trabajo de Tecnología, Innovación y Conocimiento (FACTTIC).</p><p><strong>Nuestra gestora de contenidos en cifras:</strong>
Al 30 de junio 2021 hay más de 300 sitios creados a través de Sutty. En estos seis meses los sitios recibieron <strong>millones de visitas</strong>. El <strong>95%</strong> de esas visitas se resolvieron en <strong>0 (cero) milisegundos</strong> (“más rápido que inmediatamente” diría una comunicadora local). Otro <strong>4%</strong> de las visitas se respondió en <strong>menos de 100 milisegundos</strong>:
 una tasa 3 veces más rápida que los 300 milisegundos que indica Google 
en sus Google vitals como tope para cargar un sitio y que no resulte 
“lento”.</p><figure data-multimedia=""><img data-multimedia-inner="" src="https://panel.sutty.nl/rails/active_storage/blobs/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBBaTBnIiwiZXhwIjpudWxsLCJwdXIiOiJibG9iX2lkIn19--9785f4800a8aabe25855971cae6f417795cc164d/cifras-reporte1.png" alt=""><figcaption>95% de las visitas a nuestros sitios se resuelven en 0 cero milisegundos: carita sonriente</figcaption></figure><figure data-multimedia=""><img data-multimedia-inner="" src="https://panel.sutty.nl/rails/active_storage/blobs/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBBaThnIiwiZXhwIjpudWxsLCJwdXIiOiJibG9iX2lkIn19--3c9b30f02b7ea3d3c71566f4f04c170114a2829b/cifras-reporte2.png" alt=""><figcaption>4% de las restantes visitas se resuelven en menos de 100 milisegundos. 3 veces más rápido que el tope de Google vitals.</figcaption></figure><p><strong>Contribuciones</strong>:
Dimos una charla en el <strong>Festival Latinoamericano de Instalación de Software Libre, FLISOL ranchero en Ciudad de México</strong>, abril 2021, que pueden ver aquí <a href="https://archive.org/details/sutty-flisol-2021">https://archive.org/details/sutty-flisol-2021</a></p><p><a>Realizamos más de <strong>19 colaboraciones a proyectos de Software libre</strong>
 que utilizan Jekyll. Creamos y aportamos 38 plugins / </a>complementos que 
resuelven o mejoran asuntos específicos de Jekyll, y nuestros aportes de
 optimización fueron incluidos en la versión 4.2, <a href="https://github.com/jekyll/jekyll/releases/tag/v4.2.0">https://github.com/jekyll/jekyll/releases/tag/v4.2.0</a> su último lanzamiento.</p><p><strong>Formación</strong>:
Participamos de dos cursos de cooperativismo de plataforma:</p><ul>
<li>
<em>Cooperativismo de Plataforma</em> de Mondragón + New School <a href="https://platform.coop/blog/platform-co-ops-now-2nd-edition/">https://platform.coop/blog/platform-co-ops-now-2nd-edition/</a>
</li>
<li>
<em>Innovación abierta para ámbitos cooperativos y organizaciones de la economía social y solidaria</em> de Instituto Nacional de Asociativismo y Economía Social (INAES)
</li>
</ul><p><strong>Alianzas</strong>:</p><ul>
<li>
Conocimos y nos aliamos con<a href="https://hypha.coop/"> Hypha Coop</a> y <a href="https://distributed.press/">Distributed Press</a> para empezar a bancar la web distribuida en Sutty
</li>
<li>
Recibimos una donación de compas de <a href="http://anagora.org">anagora.org</a> para apoyar nuestro trabajo.
</li>
<li>
Seguimos trabajando con <a href="cooperativadedisenio.com">Cooperativa de Diseño</a> y nos aliamos con <a href="https://www.collabo.studio/">Collabo Studio</a>.
</li>
</ul><ol><li id="fn1">¿Se preguntan qué es la <em>modalidad ayuda mutua</em>? Quienes crean su tienda mediante la gestora de Sutty, <strong>no deben pagar un costo mensual</strong> por la misma <strong>hasta alcanzar un mínimo nivel de facturación propio</strong>.
 Creemos que este acuerdo ayuda a las iniciativas comerciales 
incipientes a desarrollarse y sostenerse durante las posibles crisis o 
fluctuaciones en sus ingresos.
</li></ol>
---
title: Radios Comunitarias en Sutty
description: Lanzamos una nueva plantilla orientada a la comunicación alternativa
  y popular
author:
- Sutty
image:
  description: ''
  path: 
categories:
- Plantilas
order: 18
locales:
- cb42bc4f-ef83-4eca-b1f0-10ad3e5260e1
layout: post
uuid: 87fbfd69-fc64-441f-802d-93dd1feb353d
liquid: false
usuaries:
- 1
---



Ya está disponible en Sutty una plantilla para alojar radios comunitarias.  Les invitamos a visitar el [sitio de demostración](https://radio.sutty.nl/).

Desarrollamos esta plantilla en base a conversaciones que fuimos teniendo con compas comunicadorxs, especialmente de la [Red Nacional de Medios Alternativos](https://rnma.org.ar/).  La presentamos el 29 de agosto de 2020 durante el [17mo Encuentro de la Comunicación Comunitaria, Alternativa y Popular](https://www.instagram.com/p/CEdF80hJj5e/), donde participamos en la mesa de Software Libre.

## Características

Con esta plantilla pueden:

* Agregar todas las direcciones del *streaming* de la radio, de forma que si una esta caída, la radio siga funcionando.

* Al navegar por el sitio la radio no se corta.

* En celulares Android, al cambiar de app o apagar la pantalla, la radio sigue reproduciendo, ¡pueden escuchar la radio con el celular en el bolsillo!

* Si hay un corte, la radio retoma la transmisión.

* Publicar artículos, ilustrados con imágenes.

* Subir su logo, presentación, redes sociales y cambiar los colores para adaptarlos a su identidad.

* Todos los artículos se pueden compartir a redes sociales.

* ¡Traducir el sitio a distintos idiomas!

## ¡Queremos usarla!

Como Sutty, esta plantilla es libre y gratuita de usar, solo tienen que [registrarse en el panel](https://panel.sutty.nl/) y crear el sitio siguiendo los pasos.  Sólo pedimos que respeten nuestros [términos de servicio](https://sutty.nl/terminos-de-servicio/) y [código de convivencia](https://sutty.nl/codigo-de-convivencia/), en resumen, que sean proyectos de la comunicación popular anti-opresivos 😁

## Economía solidaria

Sutty es parte de la economía solidaria y se sostiene en base a proyectos de desarrollo específico y aportes voluntarios.  Esta plantilla la desarrollamos con le compa [Librenauta](https://copiona.com/) en 15 horas.

Tenemos un montón de ideas sobre cómo mejorar esta plantilla y características nuevas y seguramente uds. tengan las propias también.

Nuestro objetivo es que entre todes podamos pensar cómo puede ser aliada de la comunicación alternativa, dimensionando y apoyando el trabajo que realizamos desde Sutty.

¡Les invitamos a probarla y ponerse en contacto con nosotres!
---
categories:
- Sutty
title: Códigos para compartir
author:
- Sutty
layout: post
permalink: codigo-de-convivencia/
uuid: 6e0d60aa-9cc3-48f8-a603-07b61bb19750
locales:
- f1b57c69-1e1a-4e78-b288-42c5af0c5ad0
---

# Códigos para compartir

> Este código de convivencia está basado en los "[Códigos para
> compartir, hackear, piratear en
> libertad](https://utopia.partidopirata.com.ar/zines/codigos_para_compartir.html)"
> publicados por el [Partido Interdimensional
> Pirata](https://partidopirata.com.ar/).

> Utilizamos preferentemente la 'e' para referirnos a las personas en
> general. En ese sentido, alentamos las diferentes formas, estrategias
> y herramientas para incorporar prácticas no antropocéntricas,
> sexistas, ni cisexistas en la lengua. Otras alternativas que apoyamos
> --y eventualmente usamos-- son el uso del femenino, la letra e, arrobas,
> equis, asteriscos, etc.

## Introducción

Este es un ejemplo de código que busca aportar un marco de consenso para
garantizar la asistencia, permanencia y cómoda estadía de todas las
personas que habitan y utilizan Sutty, así como para bienvenir a nueves
usuaries y potenciales aliades. Para esto, fija un piso de conductas
deseables, aceptables, indeseables y/o intolerables para la comunidad.
Podés usarlo sin cambios o modificarlo para adaptarlo a tus actividades.
Este código está en permanente mutación colectiva y se alimenta, copia e
inspira de las siguientes fuentes:

* <https://geekfeminism.wikia.com/wiki/Conference_anti-harassment/Policy>

* <https://trans-code.org/code-of-conduct/>

* <https://openhardware.science/logistics/gosh-code-of-conduct/>

* <https://hypatiasoftware.org/code-of-conduct/>

Procuramos mantener y fomentar una comunidad abierta, que invite y logre
la participación de cada vez más personas, en toda su diversidad.
Sabemos que los espacios de Software Libre, informática, sistemas, etc.
son habitados mayormente por varones cis, blancos y de clase media, pese
al reconocimiento de la necesidad de eliminar la brecha de géneros. En
este sentido, este es nuestro pequeño aporte, hecho de prácticas
colectivas de múltiples dimensiones, reflexiones, lecturas, experiencias
que crecen día a día.

## Que todes les seres sean bien tratades

Cada ser con el que compartamos el espacio es merecedore de buen trato,
respeto y compasión. En otras palabras, compartimos a continuación los
criterios básicos de presentación y cuidados.

Para les humanes

Todes somos dignes de cuidados y de saludos y tenemos derecho a suponer
las buenas intenciones de le otre.

Para referirnos a otres humanes, trataremos de ser cuidadoses y
respuestuoses de su identidad de género. Para ello, son útiles los
siguientes principios:

* No presuponer, juzgar o "interpretar" el género de le otre.

* No generizar de antemano. Se desprende del punto anterior, pero hace
  especial énfasis en comportamientos naturalizados de generización (EJ:
  presuponer que porque una persona usa un vestido se nombra en
  femenino...). La propuesta es desecharlos.

* Si la persona explicita sus pronombres y modos en que quiere ser
  referenciade, lo respetamos, escuchando y procurando referirnos a elle
  usando sus pronombres elegidos.

* Si no se incluye en la presentación los pronombres preferidos, podemos
  preguntar respetuosamente qué pronombres se usan. ¡Pero atención!  Es
  una pregunta que debe dirigirse a todes por igual, de lo contrario,
  carga la "sospecha" sobre la persona señalada y puede resultar en una
  forma de hostigamiento.

* ¿Es necesario conocer el género de una persona para relacionarnos o
  referirnos a ella? Quizás una buena práctica es evitar generizar para
  todas las personas. Pero si esto implica el uso compulsivo de la "e"
  para todes, puede ser que alguna persona se sienta molesta.  (Por
  ejemplo, las personas trans\* que se identifican en femenino o
  masculino suelen sentirse molestas y "sacadas del clóset" u *outeadas*
  si se refieren a ellas con la "e", ¡en especial si son las únicas en
  ser generizadas de esta forma en un grupo!).

* Ante cualquier duda, preguntar respetuosamente y disculparse
  respetuosamente es una buena idea para ayudar a cuidarnos.

## Puntos importantes para garantizar que nuestro espacio no resulte expulsivo

**Escucharnos a todas y entre todas en un clima de cuidados**

* Escuchar lo que cada quien tiene para decir, conscientes de que todes
  tenemos algo valioso para comunicar(nos).

* Para la escucha activa, preferimos preguntar primero, en lugar de
  hacer juicios.

* Hacer silencio a veces es la condición para que otres puedan animarse
  a hablar. Escuchar es un ejercicio que requiere práctica.  También lo
  es hablar.

* Nos interesa lo que todes tengan para decir. Por lo tanto, si estás
  más entrenade en el ejercicio de participar, hablar, opinar, tené en
  cuenta que quizás haya otres que no lo estén tanto: darles el espacio
  si quieren tomarlo. ¡Pero recordá que incentivar no es lo mismo que
  presionar!

* Tratamos de revisar y discontinuar alguna práctica que pueda haber
  resultado ofensiva, para sumar al clima de respeto. Sin embargo, esto
  no significa "bajar la cabeza" o estar necesariamente de acuerdo. Al
  menos, fija un piso de respeto para comenzar un diálogo en el caso en
  que sea necesario.

* Es --al menos-- una falta de respeto repetir un comportamiento dañino
  que ya se identificó como tal. Puede incomodar, lastimar y expulsar a
  otres, por lo que preferimos llamar la atención sobre este punto todas
  las veces que sea necesario y tolerable.

* Evitamos esto nosotres y ayudamos a otres a darse cuenta cuando lo
  están haciendo.

* En los casos en los que los llamados de atención resulten
  insuficientes, hemos de revisar estos acuerdos para sostener la
  convivencia. Eso implica actuar de acuerdo a ellos. Y también que
  estos códigos pueden ser revisados y actualizados en caso de que se
  considere necesario (deje de haber consenso).

* Una manera en la que los espacios de Software Libre y tecnologías
  pueden y suelen ser expulsivos es mediante actitudes que no contemplan
  la diversidad de saberes e interlocutor\*s. So pretexto de incluir
  tecnicismos, muches compañeres quedan al margen de lo que está
  sucediendo, muchas veces, sin que nadie tenga la mínima delicadeza de
  verificar que todes estén siguiendo la conversación.

  Recomendamos fervientemente estar atentes a estas dinámicas para poder
  evitarlas y/o revertirlas.

* La otra cara de la situación anterior es el famoso _mansplaining_: un
  tipo cis poniéndose en el lugar de la autoridad del saber para
  (sobre-)explicar todo a le otre, de manera paternalista y sin tener en
  cuenta lo que le otre quiere o no escuchar, decir, lo que sabe o hace,
  etc.

* Creemos que no hace falta ser "una voz autorizada" para opinar y
  participar. La cultura libre se comparte entre todes.

* "Compartir es bueno" vs. "Google es tu amigo".  La meritocracia y
  ciertos códigos tradicionales de ciertas ciber-comunidades suelen
  operar de manera contraria a la propuesta de la cultura libre de
  compartir. Apoyamos la cultura piratil que suma más piratas a los
  barcos. Creemos que la cultura es para todes y desafiamos las
  prácticas elitistas.

* No damos por sentado que la persona con la que estamos interactuando
  comparte gustos, creencias, pertenencias de clase, sexualidad, etc.
  Podemos ser violentes si hacemos una lectura equivocada de le otre.
  Recomendamos siempre preguntar de manera respetuosa y evitar
  comentarios o chistes que puedan herir a les otres.

* Usamos lenguaje amable e inclusivo y mostramos conductas amables e
  inclusivas.

* Respetamos los diferentes puntos de vista, experiencias, creencias,
  etc. y lo tenemos en cuenta cuando estamos en grupo para verlo
  reflejado en nuestras actitudes.

* Aceptamos las críticas. En especial las constructivas ;)

* Nos enfocamos en lo que es mejor para la comunidad, sin por ello
  perder de vista la calidez, el respeto y la diversidad entre cada une
  de nosotres.

* Mostramos empatía con les otres. Queremos comunicarnos y compartir.

* Es útil tener en cuenta que las personas tenemos capacidades,
  historias, recorridos... diferentes. Es posible que algunos
  comentarios no sean comprendidos. Trataremos de evitar la mala fe y
  sumar todas las herramientas de accesibilidad para todas las personas.

* El punto anterior incluye a personas neurodiversas y con experiencias
  de trauma. A veces el sarcasmo o la ironía no es bien recibido o
  comprendido por todes. Será útil tenerlo en cuenta para buscar
  estrategias que no excluyan a las personas de nuestros intercambios.
  Por otro lado, si creemos que determinados temas pueden ser sensibles
  (desencadenantes de recuerdos, fobias, difíciles de tolerar o cargados
  de violencia o imágenes corporales muy explícitas, por ejemplo) para
  algunas personas y nos valemos de las advertencias de contenido o
  _content warning_ (cw) (ej: "cw: comentarios de violencia sexual y
  violencia física") antes del contenido a introducir. Esto permite que
  cada cual pueda elegir si acceder o no a esos contenidos y que no le
  tomen por sorpresa.

* Respetamos los límites que establecen otras personas (espacio
  personal, contacto físico, ganas de interactuar, no querer dar datos
  de contacto o ser fotografiades, etc.)

* ¡Queremos y (creemos) en sumar piratas!

## Consentimiento para documentar o compartir en medios

* Si vas a publicar video o fotos, obtené el consentimiento de las
  personas.

* Si hay menores, consultalo con su familia responsable.

## Nuestro compromiso contra el acoso

En el interés de fomentar una comunidad abierta, diversa y hospitalaria,
nosotres como contribuyentes y administradores nos comprometemos a hacer
de la participación en nuestro proyecto y nuestra comunidad una
experiencia libre de acoso para todes, independientemente de la edad,
diversidad corporal, capacidades, neuro-diversidad, etnia, identidad y
expresión de género, nivel de experiencia, nacionalidad, apariencia
física, raza, religión, identidad u orientación sexual y otras.

Ejemplos de comportamiento inaceptable por parte de participantes:

* Comentarios ofensivos relacionados con el/los género/s, la identidad
  y expresión de género, la orientación sexual, las capacidades, las
  enfermedades mentales, la neuro(a)tipicalidad, la apariencia física,
  el tamaño corporal, la raza o la religión.

* Comentarios indeseados relacionados con las elecciones y las prácticas
  de estilo de vida de una persona, incluidas, entre otras, las
  relacionadas con alimentos, salud, crianza de les hijes, drogas y
  empleo.

* Comentarios insultantes o despectivos (_trolling_) y ataques
  personales o políticos.

* Dar por sentado el género de las demás personas.  En caso de duda,
  preguntá educadamente por los pronombres. No uses nombres con los que
  las personas no se identifican, usá el nombre, _nickname_ o apodo que
  hayan elegido (¿Realmente necesitás el nombre y el número de DNI,
  datos biométricos, carta natal, etc.?).

* Comentarios, imágenes o comportamientos sexuales innecesarios o fuera
  de lugar en espacios en los que no son apropiados.

* Contacto físico sin consentimiento o reiterado tras un pedido de cese.
  En el mismo sentido, invasión del espacio corporal (y espacios en
  general).

* Amenazas contra otras personas.

* Incitación a la violencia contra otra persona, que también incluye
  alentar a una persona a autolesionarse.

* Intimidación deliberada.

* Acechar (_stalkear_) o perseguir.

* Acosar fotografiando o grabando sin consentimiento, incluyendo también
  subir información personal a Internet sobre alguien para acosarle.

* Interrumpir constantemente en una conversación.

* Hacer comentarios sexuales indeseados.

* Patrones de contacto social inapropiados, como por ejemplo
  pedir/suponer niveles de intimidad inapropiados con les demás.

* Seguir tratando de entablar conversación con una persona cuando se te
  pidió que no lo hagas.

* Divulgar deliberadamente cualquier aspecto de la identidad de una
  persona sin su consentimiento, excepto que sea necesario para proteger
  a otras personas de abuso intencional.

* Hacer pública una conversación privada de cualquier tipo.

* Otros tipos de conducta que pudieran considerarse inapropiadas en un
  entorno de camaradería.

* Reiteración de actitudes que les participantes señalen como ofensivas
  o violatorias de este código.

## Consecuencias

* Se espera que la persona a la que se la haya pedido que cese un
  comportamiento que infringe este código acate el pedido de forma
  inmediata, incluso si no está de acuerdo con este.

* Les administradores pueden tomar cualquier acción que juzguen
  necesaria y adecuada, incluyendo expulsar a la persona o dar de baja
  sus sitios sin advertencia. Esta decisión la toman les administradores
  en consenso y se reserva para casos extremos que comprometan la
  continuidad de la comunidad o bien la posibilidad de permanencia en
  ella de otres participantes sin sentirse agraviades o amenazades.

* Les administradores se reservan el derecho a prohibir la asistencia a
  cualquier actividad futura o publicación de sitios.

Como mencionamos antes, este código está en permanente mutación
colectiva. El objetivo principal es generar un ambiente inclusivo y no
expulsivo, un ambiente transparente y abierto en el que no haya
escalones faltantes ("el escalón que falta en la escalera y todo el
mundo sabe y avisa pero nadie se quiere hacer cargo"). Es importante que
se adapte a las actividades y se nutra de las contribuciones de les
usuaries.  Recibir tus comentarios y aportes nos ayudará a cumplir con
su objetivo principal.

## ¡Sigamos en contacto!

Si pasaste por alguna situación que quieras compartir --te hayas animado
o no a decirlo en el momento--, podés ponerte en contacto con nosotres.

Con respecto a quejas o avisos acerca de situaciones de violencia,
acoso, abuso o repetición de conductas que se advirtieron como
intolerables, tomamos la responsabilidad de tenerlas en cuenta y
trabajar en ellas para que el resultado sea el favorable al espíritu de
colectiva que elegimos y describimos aquí.  Si bien consideramos que las
prácticas punitivistas no van con nosotres, nuestra decisión explícita
es escuchar a la persona que se manifiesta como violentada o víctima y
acompañarla.

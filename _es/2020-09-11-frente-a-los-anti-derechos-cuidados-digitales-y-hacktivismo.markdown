---
title: 'Frente a los anti-derechos: cuidados digitales y hacktivismo'
description: Lanzamiento del sitio ConVozContamos.com
author:
- Sutty
image:
  path: public/ccaegxtpho6by76z254vb82dptve/Contamos con voz.png
  description: Captura de la portada del sitio
permalink: lanzamiento-convozcontamos/
categories:
- Sitios
tags: []
draft: false
order: 19
locales: []
layout: post
uuid: 52be1f3c-565f-437c-9a59-e7e87d74a730
liquid: false
usuaries:
- 1
last_modified_at: 2022-06-02 17:29:48.957875138 +00:00
---

<blockquote><p>"Pensamos esta página, ConVozContamos como un espacio seguro donde mujeres, adolescentes, personas con capacidad de gestar, profesionales de salud u otras personas involucradas puedan contar sus experiencias con sus abortos o los abortos que acompañaron. Este sitio es un espacio seguro para todas nosotras, para contar nuestras experiencias con nuestros abortos, para revelar los obstáculos que enfrentamos cuando queremos acceder a nuestros derechos y cómo recibimos ayuda." -- <a href="https://convoz.sutty.nl">https://convoz.sutty.nl</a> <del><a href="https://convozcontamos.com/">Con Voz Contamos</a></del></p></blockquote><p>Cuando nos convocaron para participar de la construcción de la web ConVozContamos, nos enorgulleció poder aportar la tecnología web para crear un espacio realmente seguro. Porque a pesar de que la ILE es un protocolo habilitado por el Código Penal argentino hace 99 años, la mirada moralista sobre los cuerpos gestantes tiene un ejército de anti-derechos que odian y persiguen esta realidad. Y su activismo atraviesa los espacios físicos tanto como los virtuales. Los ataques en línea hacia activistas y periodistas surgen de grupos organizados políticamente, con recursos económicos, que pueden pagar trolls y campañas de ataque, censura o tergiversación de la información.</p><p>Por eso programamos el sitio web de la iniciativa del <a href="http://www.ela.org.ar/">Equipo Latinoamericano de Justicia y Género (ELA)</a>, la <a href="http://www.redaas.org.ar/">Red de Acceso Al Aborto Seguro de Argentina (REDAAS)</a> y el <a href="http://www.cedes.org/">Centro de Estudios de Estado y Sociedad (CEDES)</a>, en colaboración con <a href="http://oratorialab.com/">OratoriaLab</a> y la <a href="http://cooperativadedisenio.com/">Cooperativa de Diseño</a>, con todo un sistema de cuidados digitales.</p><p>Algunas de las funcionalidades de Sutty que protegen este sitio y a sus visitantes:</p><ol>
<li>
El sitio es transportable, se pueden distribuir copias de seguridad si hubiera algún ataque.
</li>
<li>
El sitio es estático y por lo tanto mucho menos vulnerable: no hay un gestor de contenidos potencialmente vulnerable a ataques de denegación de servicio (DOS y DDOS), modificación de contenido (<em>deface</em>) y otros. No se puede atacar directamente.
</li>
<li>
La única posibilidad de dañar el sitio es ingresar al servidor y modificar sus archivos, proceso mucho mucho mucho más difí­cil, más laborioso y previsto en nuestras medidas de auto-defensa.
</li>
<li>
El sitio es liviano, los videos y animaciones convertidos a formatos más ligeros y otras optimizaciones hacen que utilice menos recursos de Internet y el navegador (y posicione mejor en las búsquedas web)
</li>
<li>
Previendo la actividad de grupos de trolls y anti-derechos, conocidos por organizarse para inundar los formularios de contacto, desarrollamos una técnica para enviar mensajes que impide la automatización de un trolleo.
</li>
<li>
Sutty también protege la privacidad de quienes visitan el sitio, ya que no registramos información de las personas que navegan este sitio web. Así prevemos que puedan exponerse a otros tipos de violencia.
</li>
<li>
No guardamos las historias enviadas en el formulario de contacto del navegador ni en el servidor, la comunicación entre personas que cuentan su historia y el equipo de #ConVozContamos es directa.
</li>
</ol>
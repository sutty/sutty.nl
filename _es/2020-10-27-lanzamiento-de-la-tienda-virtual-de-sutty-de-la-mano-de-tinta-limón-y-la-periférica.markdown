---
title: Lanzamiento de la Tienda Virtual de Sutty, de la mano de Tinta Limón y La Periférica
description: Presentamos el carrito de compras de Sutty, desarrollado en base a Spree
  para poder ser creado y gestionado desde nuestra plataforma segura, veloz y ecológica.
author:
- Sutty
image:
  path: public/au20gdedl2iib6vg5lz3km356ljv/Screenshot_2020-10-27 Tinta Limón Ediciones.png
  description: Parte de la portada del sitio de Tinta Limón, en la que se ve el ícono
    del carrito de compras de Sutty activo.
order: 20
layout: post
uuid: 52bd4946-e33c-4450-ba1a-ed4a1a06f363
liquid: false
usuaries:
- 5
---



# En este equipo de trabajo hacemos sitios web, aplicaciones nativas para celulares, y ahora también (🥁 drumroll...) **¡tiendas virtuales!**

## Estamos muy contentes de haber **creado el carrito de compras para la [Editorial Tinta Limón](https://tintalimon.com.ar/#rosa-luxemburgo-y-el-arte-de-la-pol%C3%ADtica), con diseño de la [Cooperativa de Diseño](https://cooperativadedisenio.com/)**. Así sentamos las bases para ofrecer plataformas para compra orientadas al **comercio justo, las economías solidarias y los proyectos autogestivos** (nuestros favoritos del mundo).

\- Pero, Sutty, ¿cómo es que una plataforma de compra se orienta al **comercio justo**, más allá de una bonita declaración de principios o predilecciones?

Estamos en tiempos de desesperación pandémica, con millones de negocios cambiando su modelo físico por uno digital. Están surgiendo nuevos proveedores de servicios de “tiendas virtuales” de la mano de los gigantes de siempre: Telecom, MercadoLibre, etc. Este software envuelto en una interfaz de diseño adictivo, se inserta entre quienes ofrecen servicios y quienes están en su búsqueda, encajando procesos extractivos en interacciones sociales. Su eficiencia está construida en torno a la extracción de valor en beneficio de accionistas y propietarios: intermediaciones capitalistas que explícitamente se benefician de los bajos salarios del sector de servicios, de la desigualdad económica, y el desmoronamiento de los derechos de les trabajadores.

En Sutty soñamos con aportar al crecimiento del **[Cooperativismo de plataforma](https://endefensadelsl.org/cooperativismo_de_plataforma.html)**: “**modelos democráticos de propiedad para la Internet, (capaces de) llevar equidad laboral a las plataformas laborales.**” (Trebor Scholz en *Cooperativismo de plataforma*). **A través de nuestra tienda virtual, no buscamos ser intermediaries capitalistas,** sino **crear plataformas que sean gestionadas por sus trabajadorxs y, de a poco, por sus usuaries, apuntando a que el proceso formal de toma de decisiones llegue a incluirles también.** Un proyecto que usará el excedente para desarrollar features que decidamos colectivamente, preguntando a los emprendimientos qué necesitan para seguir creciendo, y programando según esa brújula.

Hoy les invitamos a conocer el carrito de compras de [Tinta Limón](https://tintalimon.com.ar), y a difundir esta solución digital que tiene una raíz distinta, con ganas de brotar para todxs.

<https://www.tintalimon.com.ar/>

“¡Una Internet del pueblo es posible! Una coalición de diseñadorxs, trabajadorxs, artistas, cooperativas, desarrolladorxs, sindicatos con inventiva y defensorxs de les trabajadorxs puede cambiar las estructuras para que todes podamos cosechar los frutos de nuestro propio trabajo.” Trebor Scholz.

> Con esta plataforma cooperativa, podés tener tu carrito de compras sin comisiones, sin intermediaciones y con propuestas accesibles. 
>
> ¡Escribinos a [hi@sutty.nl](hi@sutty.nl) o a través de nuestro [formulario](https://sutty.nl/index.html#contacto) para empezar a trabajar juntes!
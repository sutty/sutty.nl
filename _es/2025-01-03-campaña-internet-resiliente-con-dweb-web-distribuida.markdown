---
title: 'Campaña: Internet resiliente con DWeb (Web Distribuida)'
description: Recuperá tu página web, recuperá tu historia. Beca completa para organizaciones,
  colectivos y activistas de DDHH, defensa del territorio, transfeministas y/o economía
  social. Convocatoria abierta hasta el 13/01/2025
author:
- Sutty
- Distributed Press
- Hypha
image:
  path: public/4w7xs4b6j4af3k19oqjum6dapeyl/horizontalES1.png
  description: 'Campaña: Internet resiliente con DWeb (Web Distribuida). Recuperá
    tu página web, recuperá tu historia. Distributed Press, Sutty y Hypha.'
categories: []
tags: []
draft: false
activity: true
order: 37
locales:
- 96c7320f-bb72-45c8-8ca5-709034150976
layout: post
uuid: a31598a6-899b-46d8-b4bc-76d38d150456
render_with_liquid: false
usuaries:
- 1
- 310
created_at: 2025-01-03 17:42:14.313501676 +00:00
last_modified_at: 2025-01-14 21:41:02.285969845 +00:00
---

<h3 style="text-align:start" id="recupera-tu-pagina-web-recupera-tu-historia"><strong>Recuperá tu página web, recuperá tu historia</strong></h3>
<p style="text-align:start">En el mundo digital, no hay nada más desalentador que perder el acceso a una página web que hemos dedicado tiempo y esfuerzo en construir, que es nuestra punto de encuentro con nuestrxs lectores y está referenciada por artículos y noticias :(</p>
<h4 style="text-align:start" id="perdiste-una-pagina-web"><strong>¿Perdiste una página web?</strong></h4>
<p style="text-align:start">Beca completa para organizaciones, colectivos y activistas de DDHH, defensa del territorio, transfeministas, economía social.</p>
<p style="text-align:start"><strong>Recuperamos tu sitio y lo hacemos más resiliente con DWeb, 100% gratis.</strong></p>
<p style="text-align:start">¡Inscribite! Elegiremos dos casos con los que trabajar en enero/febrero 2025.</p>
<p style="text-align:start"><mark style="color:inherit;background-color:#a5f7fd"><a href="https://sutty.nl/formulario-de-recuperacion/" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">Completa el formulario de inscripción para postularte</a>.</mark></p>
<h4 style="text-align:start" id="faqs"><strong>FAQs</strong></h4>
<h5 style="text-align:start" id="como-lo-recuperan"><em>¿Cómo lo recuperan?</em></h5>
<p style="text-align:start">Podemos restaurar tu sitio si cumple con ciertas condiciones técnicas. <a href="https://sutty.nl/formulario-de-recuperacion/" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">Contanos tu situación en el formulario de inscripción</a> para que podamos evaluarlo.</p>
<h5 style="text-align:start" id="como-lo-hacen-mas-resiliente"><em>¿Cómo lo hacen más resiliente?</em></h5>
<p style="text-align:start">En <a href="https://distributed.press/" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">Distributed Press</a>, donde construimos en conjunto las cooperativas <a href="https://sutty.nl/" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">Sutty </a>y <a href="https://hypha.coop/" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">Hypha</a>, nos dedicamos a crear <strong>versiones de sitios en protocolos decentralizados - IPFS y Hypercore.</strong></p>
<p style="text-align:start">Tener copias de tu sitio <em>hosteadas</em> colaborativamente por fuera de HTTP te evita fragilidad y linkrot, disminuye el riesgo de perder información, hace tu data más resistente a la censura y borrado, y permite que esté disponible aún si los servidores de <em>hosteo</em> se caen. <strong>En esta beca, además de restaurar tu sitio, lo alojaremos en la Web Distribuida.</strong></p>
<h5 style="text-align:start" id="cuales-son-las-condiciones"><em>¿Cuáles son las condiciones?</em></h5>
<p style="text-align:start">Tenemos la posibilidad de recuperar 1 sitio de 2 organizaciones / personas durante febrero 2025. El criterio de selección incluye aspectos técnicos que hagan posible la restauración del sitio, y darnos consentimiento para compartir la historia como caso de estudio al finalizar. Además, por los ajustados tiempos de este proyecto, deberán acordar a tener tiempos de respuesta de alrededor de 48hs ¡así podemos avanzar juntes!</p>
<p style="text-align:start"><strong><a href="https://propuestas.sutty.nl/recupera-tu-sitio/" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">Conocé más acerca de las condiciones y alcance de esta campaña</a></strong></p>
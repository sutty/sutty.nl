---
title: 'Preservando la memoria colectiva a través de la recuperación de sitios: el
  caso de Commons Transition Primer'
description: "¿Cómo nos aseguramos que el conocimiento compartido no se pierda en
  el tiempo? En Sutty desarrollamos estrategias y tecnologías para almacenar, fortalecer
  y preservar los contenidos que desarrollamos y los de organizaciones queridas. En
  esa línea, nos conectamos con el equipo de DisCO para colaborar en la recuperación
  de un sitio web clave que habían desarrollado hace unos años y cuyo acceso habían
  perdido."
author: []
image:
  path: public/2plwc6n2fvlmlw6kn1nym1ejbiku/prime.png
  description: Captura del sitio Commons Transition Primer, recuperado en enero 2025
categories:
- Sitios
- Técnica
tags: []
draft: false
activity: true
order: 40
locales: []
layout: post
uuid: 1fe9a7f7-7333-4589-8c62-ea20ff5048f1
render_with_liquid: false
usuaries:
- 310
created_at: 2025-02-12 12:55:08.235413350 +00:00
last_modified_at: 2025-02-12 13:09:27.967777864 +00:00
---

<p style="text-align:start">En la era del "exceso de información" tendemos a confiar en que el contenido digital estará siempre disponible, pero la realidad es más frágil de lo que parece. <strong>Los links se rompen, los servidores se caen y el conocimiento colectivo que alguna vez estuvo al alcance de un clic <a href="https://youtu.be/BAuqozi64WQ?si=pd5b6iUKuwQHoZ5H" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">puede… desaparecer</a></strong>. Este fenómeno se conoce como "link rot" y es una amenaza real para la preservación de nuestra historia digital. <a href="https://www.pewresearch.org/data-labs/2024/05/17/when-online-content-disappears/" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">Un estudio reciente de Pew Research Center</a> reveló que 1 de cada 3 sitios web desaparecen en una década.</p>
<p style="text-align:start">Entonces, <strong>¿cómo nos aseguramos que el conocimiento compartido no se pierda en el tiempo?</strong></p>
<p style="text-align:start"><br>
  En Sutty nos encontramos desarrollando mecanismos, estrategias y tecnologías para almacenar, fortalecer y preservar los contenidos que desarrollamos y los de organizaciones queridas. En esa línea, nos conectamos con el equipo de DisCO para <strong>colaborar en la recuperación de un sitio web clave</strong> que habían desarrollado hace unos años y <strong>cuyo acceso habían perdido</strong>.</p>
<h2 style="text-align:start" id="la-perdida-de-commons-transition-primer">La pérdida de Commons Transition Primer</h2>
<p style="text-align:start">DisCO (<a href="https://www.disco.coop/" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">Distributed Cooperative Organizations</a>) enfrentó este desafío cuando notó que el sitio <a href="https://primer.commonstransition.org/" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">Commons Transition Primer</a>, un recurso clave en su ecosistema, había desaparecido.</p>
<p style="text-align:start"><br>
  "<em>Gran parte del contenido de Commons Transition Primer lo escribimos entre el equipo fundador de DisCO (Ann Marie Utratel y Stacco Troncoso) basado en conceptos de la fundación P2P, pero aportando nuestra visión, incorporando una perspectiva feminista y decolonial y es como una prefiguración de DisCO</em>", explican. Cuando lanzaron el DisCO Manifesto en 2019, el sitio se convirtió en una referencia fundamental, con numerosos enlaces apuntando a él. Pero al preparar hace unos meses la edición de 6to aniversario del manifiesto y su traducción al español, se toparon con un problema: la página ya no existía. Debido a movimientos entre organizaciones, el sitio resultó perdido.</p>
<figure><img src="public/1ks9vdiy5njen4ho21swm671r8zw/prime.png" class="img-fluid" controls="false"></figure>
<p style="text-align:start"></p>
<h2 style="text-align:start" id="pasos-de-la-restauracion">Pasos de la restauración</h2>
<p style="text-align:start">La recuperación del sitio fue un proceso que combinó diferentes técnicas y herramientas. Por suerte, el administrador de sistemas había realizado backups diarios desde su lanzamiento, lo que nos permitió acceder a copias previas a cualquier problema de seguridad. Esto fue fundamental ya que el sitio, desarrollado en WordPress y sin mantenimiento desde 2019, había sido infectado en algún momento.</p>
<p style="text-align:start">El primer paso fue acceder a un <strong>backup</strong> de la época en la que el sitio estaba funcional y limpio de malware. Luego se recurrió a la <strong>Wayback Machine</strong>, descargando una copia completa del sitio desde su última captura disponible. Sin embargo, este método no fue infalible: de aproximadamente 68 páginas, se detectó que unas 14 no habían sido guardadas por la Wayback Machine.<br>
  Para cerrar estas brechas, se utilizaron <strong>herramientas especializadas en la detección de enlaces rotos</strong>, identificando los archivos faltantes. Luego, <strong>buscamos en otras fuentes</strong>, incluyendo archivos vinculados desde sitios externos, para recuperar el contenido perdido. Además, los backups disponibles fueron clave para rescatar artículos e imágenes que no estaban archivados en la Wayback Machine.</p>
<p style="text-align:start">Con todo el material recuperado, <strong>el sitio fue reconstruido como una versión estática, pre-generada o sólida</strong>, garantizando su estabilidad y reduciendo riesgos de futuras pérdidas. Esta tecnología que nosotres elegimos y recomendamos necesita muchísimo menos poder computacional, energía y costo de alojamiento pero sobre todo, disminuye a una ínfima fracción las posibilidades de hackeos ya que no hay “piezas móviles” en el servidor público y las vulnerabilidades por el software prácticamente desaparecen.<br>
  También <strong>se clonó a la web distribuida</strong> a través de <a href="https://distributed.press/" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">Distributed Press</a> para tener múltiples copias a través de <strong>protocolos distribuidos (IPFS e Hypercore)</strong>, lo cual reduce el "punto de falla único" y nos permite redirigir a otros nodos si uno de ellos dejara de funcionar o quedara inaccesible .</p>
<p style="text-align:start">Hoy <a href="https://primer.commonstransition.org/" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">Commons Transition Primer está disponible online</a> para quienes buscan comprender y construir modelos de organización cooperativa hacia una cultura más democrática y cuidadosa con el ambiente. Ésto nos da mucha alegría por el enorme valor que tiene la propuesta de DisCO para una cooperativa de trabajo (como nosotres) y para quien sea que quiera impulsar un cambio de paradigma acerca del trabajo humano. DisCO ofrece un marco para construir espacios de trabajo que incorporen cuidados y contrapoder desde el feminismo interseccional y la economía solidaria. Conozcan su enorme propuesta en <a href="https://www.disco.coop/" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">su sitio principal</a>.</p>
<h2 style="text-align:start" id="buscando-preservar-la-memoria-digital">Buscando preservar la memoria digital</h2>
<p style="text-align:start">La recuperación de Commons Transition Primer no sólo fue un rescate técnico, sino también un acto de resistencia: un esfuerzo por mantener vivas las ideas y prácticas que inspiran a comunidades en todo el mundo. La solución al linkrot está en construir una web descentralizada y resiliente donde el conocimiento compartido no pueda desvanecerse con el tiempo.</p>
<p style="text-align:start"><br>
  En Sutty trabajamos incorporando más y más herramientas que permiten re-construir y fortalecer sitios perdidos. De hecho, durante febrero 2025 en actividad con Distributed Press otorgamos una beca a dos organizaciones de DDHH que perdieron sus sitios web para restaurárselos de forma gratuita. Podrán leer acerca de ambos casos en nuestro blog a la brevedad. ¡Contáctennos si desean recuperar una página web y aprender acerca de <strong>estrategias de resiliencia digital</strong> con nosotres!</p>
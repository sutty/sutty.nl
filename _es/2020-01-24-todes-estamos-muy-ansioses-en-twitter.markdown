---
title: Todes estamos muy ansioses en Twitter
description: Nuestra experiencia con Twitter
author:
- Sutty
image:
  description: ''
categories:
- Guías
- Comunicación
tags:
- twitter
- fediverso
- guías
- reflexiones
order: 6
layout: post
uuid: 0b371e28-7f1d-4885-881b-54ea01701791
liquid: false
usuaries:
- 1
---

Nos tomó un tiempo decidir usar Twitter. Por un lado nos da un poco de rabia tener que usar plataformas corporativas para poder comunicarnos, por otro nos da ansiedad encontrarnos de golpe con mucha gente e incluso de yapa y sin advertencia de contenido, con personas que nos han violentado.

Al final y con la gran ayuda de nuestra compañera comunicadora Ana (además la creadora de [LunarApp](http://lunarcomunidad.com/)) nos decidimos porque también hay muchas cosas interesantes sucediendo (y ya que estábamos, también tenemos FOMO --miedo a estar perdiéndonos de cosas, pero no les recomendamos la explicación de Wikipedia en castellano) y para que Sutty se sostenga, tenemos que reconocernos y ayudarnos con otres :

Así que al final seguimos los [consejos piratas para la apostasía de redes sociales](https://utopia.partidopirata.com.ar/zines/apostatemos_de_las_redes_asociales.html) y [estamos en Twitter](https://twitter.com/SuttyWeb) pero seguimos en el [Fediverso](https://todon.nl/@sutty) (¡vengan!) e inspirades por el ["Que no nos acosen por Facebook"](https://utopia.partidopirata.com.ar/zines/que_no_nos_acosen_por_facebook.html), revisamos todas las opciones y nos configuramos la cuenta así:

En el sitio de la cuenta fuimos a [**Más \> Opciones y privacidad**](https://twitter.com/settings/account)


# Cuenta \> Ingreso y seguridad


**Contraseña:** Usamos una contraseña generada automáticamente con KeepassXC o algún otro gestor de contraseñas, mientras más larga mejor y no nos tenemos que agobiar recordando contraseñas :)

**Seguridad \> Autenticación en dos pasos:** La autenticación en dos pasos permite solicitar un segundo factor para poder ingresar a nuestra cuenta. Como con la tarjeta de débito en el cajero automático, tenemos la tarjeta pero sólo funciona con un PIN que sabemos. En el caso de 2FA (las siglas), sabemos nuestro correo, contraseña y además un código aleatorio que se envía o genera desde un dispositivo que tenemos (como el celular).

Es una medida extra de protección ya que solo con correo y contraseña es posible que si alguien obtiene acceso a nuestro correo pueda obtener acceso a nuestras cuentas.

Tienen la desventaja que siempre tenemos que tener acceso a nuestro dispositivo de autenticación, por lo que hicimos copias de seguridad.

**Mensaje de texto:** No recomendamos recibir códigos de autenticación por SMS, ya que es un medio inseguro (es posible interceptar los mensajes de SMS) o alguien con nuestro celular puede obtenerlos también.

**App de autenticación:** Podemos utilizar la app FreeOTP+ (disponible en [FDroid](https://f-droid.org/)) para generar códigos de autenticación. Twitter nos va a pedir que escaneemos un código QR desde la app, pero también nos guardamos el QR como una imagen en nuestro gestor de contraseñas. De esta forma tenemos un resguardo si perdiésemos acceso a nuestro celular.

**Llave de seguridad:** Es un dispositivo USB que genera códigos de seguridad automáticamente :O

**Código de backup:** Es una contraseña aleatoria que sirve como resguardo si perdemos acceso a los dispositivos de autenticación. Solo puede ser usado una vez, así que si lo llegásemos a usar, tendríamos que volver a esta sección y anotar el siguiente (¡en nuestro gestor de contraseñas!)

**Contraseña temporal:** Son contraseñas distintas a la real que podemos usar en aplicaciones que nos piden acceso a la cuenta de Twitter. ¡Así no le damos nuestra contraseña real a nadie!


# Datos y permisos


**Tus datos de twitter:** Podríamos descargar toda la información que tienen de nosotres

**Aplicaciones y sesiones:** Podemos ver todas las sesiones abiertas y cerrar las que no reconozcamos o no queramos más. Cuando veamos alguna extraña vamos a hacer una captura de pantalla, aunque Twitter no muestra las direcciones IP.


# Privacidad y seguridad \> Tuits


**Proteger tus tuits:** Activamos para que solo las personas que nos sigan puedan leer lo que decimos. Como esta es una cuenta de comunicación, la dejamos desactivada.

**Información de ubicación:** Desactivamos para no compartirle nuestra ubicación.

**Eliminar información de ubicación:** Eliminamos la que ya se hayan agarrado sin nuestro consentimiento.

**Etiquetado en fotos:** Desactivamos para no permitir a otras personas que nos identifiquen en fotos.


# Mensajes directos


**Recibir mensajes de cualquiera:** Hay que desactivar para no recibir mensajes de desconocides, pero la dejamos para que nos hagan consultas :)

**Filtro de calidad:** Twitter dice que puede filtrar mensajes que considere de "baja calidad" pero no es claro en cómo decide eso...

**Mostrar acuses de recibo:** Ver o dejar que vean si leímos un mensaje. Lo deshabilitamos porque a veces leemos cosas fuera de hora pero decidimos responder en horario laboral :P


# Descubrir


**Permitir que otras personas que sepan tu dirección de correo te encuentren en Twitter:** Si hubiéramos creado la cuenta con una dirección personal lo hubiéramos desactivado, pero nuestro objetivo es que nos encuentren así que lo dejamos activo.

**Permitir que otras personas que sepan tu número de teléfono te encuentren en Twitter:** Como todavía no tenemos un teléfono exclusivo para Sutty, este sí lo desactivamos, aunque nos hubiera gustado no tener que dárselos en primer lugar.

**Administrar contactos:** Imaginamos que si hubiésemos usado la app oficial de Twitter ya se hubieran quedado con nuestra agenda de contactos.


# Seguridad


**Mostrar medios con contenido sensible:** Lo dejamos desactivado para no leer cosas violentas sin querer

**Marcar los medios que compartimos como contenido sensible:** Lo habilitamos para no olvidarnos que si compartimos una imagen, tenemos que avisarle a otres de qué se trata y que puedan decidir si la quieren ver.

**Silenciades:** Cuentas de Twitter que silenciamos. En esta sección también podemos agregar palabras que no queremos leer. Estamos pensando cuáles.

**Cuentas bloqueadas:** Acá están las cuentas que bloqueamos, también se pueden importar listas de cuentas bloqueadas que nos compartamos. Al final bloqueamos preventivamente a cuentas que ya sabíamos que nos podíamos cruzar y le estamos pidiendo a amigues que nos avisen así no caemos en la curiosidad de leerles.


# Notificaciones


**Filtro de calidad:** Lo mismo que el anterior...

**Silenciar notificaciones:** Acá podemos elegir cuáles notificaciones no recibir para que no nos atosiguen

**Silenciades:** Lo mismo que en Seguridad. ¿Por qué querríamos recibir notificaciones de cuentas que silenciamos?

**Notificaciones push:** Las notificaciones push se envían a través de proveedores como Google y Apple (también Mozilla, para notificaciones webpush). No sabemos aún con qué nivel de privacidad Twitter envía las notificaciones, potencialmente estos proveedores podrían leer el contenido, así que recomendamos desactivarlas.

**SMS:** Desactivamos para no recibir notificaciones por SMS. Además quedan guardadas en nuestro proveedor de telefonía.

**Correo:** Desactivamos para no recibir correos o elegir cuáles queremos recibir. No habíamos hecho casi nada y ya nos habían mandado como 20 correos.


# Contenido \> Explorar


**Opciones de búsqueda:**


- **Esconder contenido sensible:** Activamos para no recibir contenido marcado como sensible en los resultados
- **Eliminar cuentas silenciadas y bloqueadas:** Activamos para no ver a la gente bloqueada en los resultados (¡por qué!)
- **Tendencias:** Desactivamos porque las tendencias pueden resultarnos sensibles (muchas lo son)

# Idiomas


Elegimos los idiomas que entendemos


# Seguridad


Lo mismo que la sección anterior


# Personalización y datos personales


Desactivamos para que Twitter no nos incluya en su análisis de perfil. Twitter nos pregunta si realmente lo queremos tener porque podríamos ver publicidades menos relevantes. Pero nosotres no queremos ver ninguna publicidad así que usamos [µBlock Origin](https://addons.mozilla.org/en-US/firefox/addon/ublock-origin/).

**Compartir tus datos con terceros:** ¡Desactivamos!

**Ver los datos de Twitter:** Podemos explorar qué cosas sabe Twitter de nosotres


# Visualización


Podemos cambiar el color y tamaño de tipografía acá. Nosotres usamos colores oscuros para protegernos la vista.


# Uso de datos

**Ahorro de datos:** Activamos para que Twitter sea menos activo

# Accesibilidad

Tenemos varias opciones de accesibilidad acá:

- **Aumentar el contraste de color:** Para poder distinguir mejor textos del fondo
- **Componer descripciones de imágenes:** Para que el lector de pantalla describa mejor las imágenes
- **Reducir movimiento:** Para que no nos mareen las animaciones, aunque igual nos mareamos con esta opción desactivada

# Clientes de Twitter

Como nunca habíamos usado Twitter personalmente, estuvimos probando varios programas para interactuar con la plataforma:

- [**Cliente web oficial**](https://twitter.com/) **:** nos marearon las animaciones y al final lo sacamos completamente porque el uso de memoria del [navegador](https://firefox.com/) pasó del 60 al 10% cuando cerramos la pestaña
- **Andstatus:**  Nos lo bajamos en el celular desde FDroid y nos gustó porque es lacónico y no mareante. Lo configuramos para que revise mensajes una vez por día.
- [**Bitlbee**](https://wiki.bitlbee.org/HowtoTwitter) **:** es un programa muy lindo que se conecta con distintos sistemas de mensajería y los convierte a mensajes de IRC, nuestro protocolo de mensajería favorito. Lo tuvimos que abandonar enseguida porque parece que Twitter cambió la forma en que permite que otros programas se conecten y las personas que desarrollan Bitlbee no tuvieron el tiempo aun.
- [**Cawbird**](https://ibboard.co.uk/cawbird/) **:** Ahora estamos probando este y nos resulta cómodo porque lo podemos usar con teclado, aunque esta diagramado para ventanas pequeñas y a algunes de nosotres nos gusta maximizarlas para no distraernos

Lo que estaríamos buscando, a ver si nos recomiendan algo, es un programa que no nos maree ni nos atosigue con información y que podamos usar cómodamente para poder seguir compartiendo lo que hacemos en Sutty y encontrándonos con compañeres. ¿Habrá alguno?  ¿Cuál nos recomiendan?

---
title: Términos de servicio
author:
- Sutty
layout: post
description: ¿Qué condiciones debemos cumplir para ser usuaries de Sutty?
permalink: terminos-de-servicio/
categories:
- Sutty
uuid: d5d3ca28-a8cb-426d-8930-e0b37db47230
locales:
- a37dfaf7-7214-4d3e-a142-b9029ea04e02
---

> Utilizamos la e como pronombre neutro para referirnos a personas
> independientemente de su identidad de género, por ejemplo "usuarie".

Este documento detalla los términos de servicio de Sutty, incluyendo
sitio web, plataforma de edición, infraestructura relacionada (salas de
chat, etc.) y sitios creados por sus usuaries a través de la plataforma,
en adelante "Sutty".

## ¡Esto es muy largo!

* Sutty están orientada a organizaciones, colectivas, campañas y
  personas que luchen por cambiar el mundo :)

* No están permitidos sitios de odio o que lo promuevan o inciten hacia
  personas por su diversidad corporal, capacidades, neuro-diversidad,
  etnia, identidad y expresión de género, nivel de experiencia,
  nacionalidad, apariencia física, raza, religión, edad, identidad u
  orientación sexual.

* No se permiten sitios que promuevan el fascismo.

* Sutty no monitorea el contenido de los sitios, pero se reserva el
  derecho a dar de baja aquellos que no respeten estos términos de
  servicio y/o códigos de convivencia.

## Quiénes pueden usar Sutty

Invitamos a todas las organizaciones, colectivos, activistas, campañas,
cooperativas, artistas y otras formas de agrupación que promuevan la
diversidad, la libertad de expresión, el transfeminismo, el
cooperativismo, la economía social, la cultura libre...

Si tenés alguna duda sobre si podés o no usar Sutty,
[escribinos](index.html#contacto).

## Quiénes no pueden usar Sutty

No están invitados fascistas, misóginos, violentos ni
\*fóbicos/odiantes.  [Consultá nuestro código de
convivencia](codigo-de-convivencia/).

Si bien Sutty no realiza monitoreo del contenido de los sitios que
aloja, se reserva el derecho a remover los sitios y usuaries que no
cumplan con estos requisitos e impedir el registro de nuevos sitios.

## Uso aceptable

Les usuaries de Sutty pueden hacer uso de todas las características de
los servicios ofrecidos, cumpliendo con los acuerdos colectivos
previstos por el código de convivencia.

Les usuaries de Sutty no pueden alojar sitios que pongan en riesgo la
continuidad del servicio y su infraestructura, especialmente aquellas
acciones que puedan colocar a Sutty en listas de bloqueo de actividades
maliciosas como _spam_, _malware_, _phishing_, etc.

El no cumplimiento de estas condiciones puede conllevar la eliminación
del sitio y cuentas de usuarie relacionadas.

## Limitación de responsabilidad

Sutty no se hace responsable por el contenido de los sitios alojados ni
las opiniones personales de sus usuaries, ni monitorea el contenido de
los sitios.

Si bien Sutty realiza copias de seguridad por su cuenta y provee
funcionalidades que permiten gestionar los cambios en los sitios, es
responsabilidad de les usuaries mantener copias de seguridad
actualizadas de sus sitios y datos de cuenta.

## Eliminación de sitios y cuentas de usuarie relacionadas

El no cumplimiento de este y otros documentos relacionados a la buena
convivencia y uso de los servicios provisto por Sutty podría resultar en
la eliminación de tales sitios y sus cuentas de usuarie relacionadas.

Les usuaries recibirán un texto que explique los motivos.

La eliminación de sitios y cuentas de usuarie quedará bajo la sola
decisión de les administradores de Sutty.

## Software libre y limitación de garantía

Sutty se desarrolla y publica como software libre bajo los términos de
la licencia AGPLv3.  Otros complementos y contenidos relacionados
podrían ser publicados bajo otras licencias.

Esto implica que todas las personas tienen acceso al código fuente y
pueden estudiarlo, modificarlo y utilizarlo sin garantías de
funcionamiento para un propósito particular.

[Acceder al código e información de
licenciamiento.](https://0xacab.org/sutty/)

---
title: Postulaciones abiertas a la Campaña Internet Resiliente con DWeb
description: Aún estás a tiempo de recuperar un sitio que hayas perdido y volverlo
  más resiliente. Aplicá a esta convocatoria de Sutty, Hypha y Distributed Press.
author: []
image:
  path: public/b13idnun50rh46u83oxyeniv2xsf/horizontalES1.png
  description: ''
categories: []
tags: []
draft: false
activity: true
order: 38
locales:
- 62f9352c-5016-4b21-964a-7c08892f9d33
layout: post
uuid: b551eb60-453d-47e1-926d-58aad4cbe8bb
render_with_liquid: false
usuaries:
- 310
created_at: 2025-01-14 22:04:50.195663640 +00:00
last_modified_at: 2025-01-14 22:18:55.554843956 +00:00
---

<p style="text-align:start">¿Alguna vez tuviste un sitio web que, con el tiempo, quedó inaccesible o desapareció? La <strong>Campaña Internet Resiliente con DWeb</strong> puede ayudarte a recuperarlo y volverlo más resiliente a través del uso de protocolos distribuidos (lo que llamamos <a href="https://sutty.nl/saber-mas-sobre-publicar-a-la-web-distribuida/" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">Web Distribuida, pueden leer más sobre ella aquí</a>)</p>
<p style="text-align:start">Si formas parte de una ONG, un colectivo o sos activista, esta convocatoria está diseñada para ayudarte a <strong>recuperar un sitio web perdido y fortalecerlo con la Web Distribuida.</strong></p>
<p style="text-align:start">El plazo para enviar tu solicitud se extendió una semana más, así que todavía tenés tiempo de postular. Si crees que esta oportunidad puede ayudarte a vos o a alguien que conoces, no dudes en <a href="https://sutty.nl/formulario-de-recuperacion/" rel="noopener" target="_blank" referrerpolicy="strict-origin-when-cross-origin">completar el formulario de postulación.</a></p>
<p style="text-align:start">Compartí esta oportunidad con quienes puedan necesitarla, las aplicaciones están abiertas hasta el 20 de enero. </p>
<figure><img src="public/t44wcit8nnauk2lnnemz1gxhl0g1/cuadradoES3.png" class="img-fluid" controls="false"></figure>
<p style="text-align:start"></p>